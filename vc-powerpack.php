<?php
/*
Plugin Name: Visual Composer Power Pack
Plugin URI: http://plugins.nucleartuxedo.com/visual-composer-powerpack/
Description: Customizes and adds extra elements to Visual Composer
Author: Milo Jennings
Version: 1.0
*/

// Set plugin version, never use more than 1 decimal, 
// to keep mathematical comparison operators functional
define('VCPP_VERSION', 1.0);
define('VCPP_PATH', plugin_dir_path(__FILE__) );
define('VCPP_URL', plugins_url('', __FILE__ ) );
define('VCPP_TEXT_DOMAIN', 'vcpp' );

add_action('init', 'visual_composer_powerpack_init');
function visual_composer_powerpack_init() {
	//Check if version variable exists, and if it doesn't match, run the upgrade
	$version_option = get_option( 'visual_composer_powerpack_version');
	if ( false === $version_option || ! isset( $version_option ) || $version_option < VCPP_VERSION ) {
		// If plugin has not been previously installed, set version to zero & set all default options 
		$current_version = isset( $version_option ) ? $version_option : 0;
		visual_composer_powerpack_upgrade( $current_version );
		update_option( 'visual_composer_powerpack_version', VCPP_VERSION );

		/* Add default option behavior here */

	}
}

function visual_composer_powerpack_upgrade( $version ){

	//if installing fresh, run full upgrade process
	if ( $version < 1 ){
		$version_update = true;
	}
	$options = get_option('visual_composer_powerpack_options');

}

// if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/lib/redux-framework/ReduxCore/framework.php' ) ) {
	// require_once( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' );
// }
if ( file_exists( VCPP_PATH . '/lib/redux-config.php' ) ) {
	require_once( VCPP_PATH . '/lib/redux-config.php' );
}

if( is_plugin_active('js_composer/js_composer.php') ){

	// $vcpp_allowed_nested_elements = array(
	// 		'aff_gal_holder',
	// 		'block_quote',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 		'zzzz',
	// 	);

	$dir = dirname( __FILE__ ) . '/vc_new_templates_dir/';
	vc_set_template_dir($dir);

	include_once('lib/scripts.php');
	//include_once('lib/additional-functionality.php');

	// Auto load all PHP files in the lib/elements folder
	foreach ( glob( VCPP_PATH . "/lib/elements/*") as $filename) { 
	    include_once( $filename );
	}

	/* ----------------------------------------------------------
	 * Remove functionality that isn't needed
	 * ---------------------------------------------------------- */
	if( function_exists("vc_remove_element") ){
		vc_remove_element("vc_twitter");
		vc_remove_element("vc_separator");
		vc_remove_element("vc_text_separator");
		vc_remove_element("vc_message");
		vc_remove_element("vc_tweetmeme");
		vc_remove_element("vc_googleplus");
		vc_remove_element("vc_pinterest");
		vc_remove_element("vc_teaser_grid");
		vc_remove_element("vc_posts_grid");
		vc_remove_element("vc_flickr");
		vc_remove_element("vc_wp_search");
		vc_remove_element("vc_wp_meta");
		vc_remove_element("vc_wp_recentcomments");
		vc_remove_element("vc_wp_calendar");
		vc_remove_element("vc_wp_pages");
		vc_remove_element("vc_wp_text");
		vc_remove_element("vc_wp_posts");
		vc_remove_element("vc_wp_links");
		vc_remove_element("vc_wp_categories");
		vc_remove_element("vc_wp_archives");
		vc_remove_element("vc_wp_rss");
	}
	vc_map( array(
		"name" => __("Admin css"),
		"base" => "admin_css",
		"show_settings_on_create" => false,
		"admin_enqueue_css" => array( plugins_url( '/assets/css/vc-admin-extend.css', __FILE__ ) ),
	));

} else {
	// Add a warning message, instructing user to install Visual Composer
}
?>