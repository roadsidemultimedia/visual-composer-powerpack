jQuery(function($){
 
$(document).ready(function() {
      $("#owl").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        itemsTablet : [768,2],
        itemsMobile : [479,1]
      });

    });  
  });