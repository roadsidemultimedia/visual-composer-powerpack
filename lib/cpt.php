<?php

add_action( 'init', 'register_cpt_vcpp_element' );

function register_cpt_vcpp_element() {

    $vcpp_element_labels = array( 
        'name' => _x( 'Elements', 'vcpp_element' ),
        'singular_name' => _x( 'Element', 'vcpp_element' ),
        'add_new' => _x( 'Add New', 'vcpp_element' ),
        'add_new_item' => _x( 'Add New Element', 'vcpp_element' ),
        'edit_item' => _x( 'Edit Element', 'vcpp_element' ),
        'new_item' => _x( 'New Element', 'vcpp_element' ),
        'view_item' => _x( 'View Element', 'vcpp_element' ),
        'search_items' => _x( 'Search elements', 'vcpp_element' ),
        'not_found' => _x( 'No Elements found', 'vcpp_element' ),
        'not_found_in_trash' => _x( 'No Elements found in Trash', 'vcpp_element' ),
        'parent_item_colon' => _x( 'Parent Element:', 'vcpp_element' ),
        'menu_name' => _x( 'Elements', 'vcpp_element' ),
    );

    $vcpp_element_args = array( 
        'labels' => $vcpp_element_labels,
        'hierarchical' => false,
        'description' => 'Elements for Visual Composer',
        'supports' => array('title'),
        'taxonomies' => array( 'vcpp_element_type', 'element_category' ),
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'can_export' => true,
        'public' => true,
        'show_in_nav_menus' => false,
        'has_archive' => false,
        'query_var' => false,
        'rewrite' => false,
        // 'capability_type' => 'switch_themes'
    );

    $vcpp_element_type_labels = array( 
        'name' => _x( 'Element Type', 'vcpp_element_type' ),
        'singular_name' => _x( 'Element Type', 'vcpp_element_type' ),
        'search_items' => _x( 'Search Element Types', 'vcpp_element_type' ),
        'popular_items' => _x( 'Popular Element Types', 'vcpp_element_type' ),
        'all_items' => _x( 'All Element Types', 'vcpp_element_type' ),
        'parent_item' => _x( 'Parent Element Type', 'vcpp_element_type' ),
        'parent_item_colon' => _x( 'Parent Element:', 'vcpp_element_type' ),
        'edit_item' => _x( 'Edit Element Type', 'vcpp_element_type' ),
        'update_item' => _x( 'Update Element Type', 'vcpp_element_type' ),
        'add_new_item' => _x( 'Add New Element Type', 'vcpp_element_type' ),
        'new_item_name' => _x( 'New Element Type', 'vcpp_element_type' ),
        'separate_items_with_commas' => _x( 'Separate Element Types with commas', 'vcpp_element_type' ),
        'add_or_remove_items' => _x( 'Add or remove Element Types', 'vcpp_element_type' ),
        'choose_from_most_used' => _x( 'Choose from most used Element Types', 'vcpp_element_type' ),
        'menu_name' => _x( 'Element Types', 'vcpp_element_type' ),
    );

    $vcpp_element_type_args = array( 
        'labels' => $vcpp_element_type_labels,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud' => false,
        'hierarchical' => false,
        'rewrite' => false,
        'query_var' => false
    );

    $vcpp_element_category_labels = array( 
        'name' => _x( 'Element Categories', 'vcpp_element_category' ),
        'singular_name' => _x( 'Element Category', 'vcpp_element_category' ),
        'search_items' => _x( 'Search Element Categories', 'vcpp_element_category' ),
        'popular_items' => _x( 'Popular Element Categories', 'vcpp_element_category' ),
        'all_items' => _x( 'All Element Categories', 'vcpp_element_category' ),
        'parent_item' => _x( 'Parent Element Category', 'vcpp_element_category' ),
        'parent_item_colon' => _x( 'Parent Element Category:', 'vcpp_element_category' ),
        'edit_item' => _x( 'Edit Element Category', 'vcpp_element_category' ),
        'update_item' => _x( 'Update Element Category', 'vcpp_element_category' ),
        'add_new_item' => _x( 'Add New Element Category', 'vcpp_element_category' ),
        'new_item_name' => _x( 'New Element Category', 'vcpp_element_category' ),
        'separate_items_with_commas' => _x( 'Separate content categories with commas', 'vcpp_element_category' ),
        'add_or_remove_items' => _x( 'Add or remove Element Categories', 'vcpp_element_category' ),
        'choose_from_most_used' => _x( 'Choose from most used Element Categories', 'vcpp_element_category' ),
        'menu_name' => _x( 'Element Categories', 'vcpp_element_category' ),
    );

    $vcpp_element_category_args = array( 
        'labels' => $vcpp_element_category_labels,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud' => false,
        'hierarchical' => false,
        'rewrite' => false,
        'query_var' => false
    );

    register_post_type( 'vcpp_element', $vcpp_element_args );
    register_taxonomy ( 'vcpp_element_type', array('vcpp_element'), $vcpp_element_type_args );
    register_taxonomy ( 'vcpp_element_category', array('vcpp_element'), $vcpp_element_category_args );

    /////////////////////////////////////////////////
    // Set default terms for taxonomies
    $default_elements = array(
        "Quote",
        "Fancy Header",
    );

    foreach ($default_elements as $title) {
        $slug = sanitize_title($title);
        wp_insert_term($title, 'vcpp_element_type', array(
            'slug' => $slug,
            )
        );
    }


    $default_content_categories = array(
        "Content",
        "Social",
        "Structure",
    );
    foreach ($default_content_categories as $title) {
        $slug = sanitize_title($title);
        wp_insert_term($title, 'vcpp_element_category', array(
            'slug' => $slug,
            )
        );
    }
    /////////////////////////////////////////////////
}

if(function_exists("register_field_group"))
{
    

    $element_options_array = array (
        'id' => 'acf_element-options',
        'title' => 'Element Options',
        'fields' => array (
            array (
                'key' => 'field_53227e1f147a9',
                'label' => 'Backend Element Options',
                'name' => '',
                'type' => 'message',
                'message' => '<h2>Backend Element Options</h2>',
            ),
            array (
                'key' => 'field_531feba13f0fe',
                'label' => 'Shortcode Tag',
                'name' => 'base',
                'type' => 'text',
                'instructions' => 'Example: 
    For a shortcode like this: [my_shortcode] 
    The shortcode tag is "my_shortcode"',
                'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fec083f0ff',
                'label' => 'Description',
                'name' => 'description',
                'type' => 'text',
                'instructions' => 'Short description of your element, it will be visible in "Add element" window',
                'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fec273f100',
                'label' => 'Class',
                'name' => 'class',
                'type' => 'text',
                'instructions' => 'CSS class which will be added to the shortcode\'s content element in the page edit screen in Visual Composer edit mode',
                'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fec3d3f101',
                'label' => 'Show settings on create?',
                'name' => 'show_settings_on_create',
                'type' => 'true_false',
                'instructions' => 'Set it to false if content element\'s settings page shouldn\'t open automatically after adding it to the stage',
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_531fec7d3f102',
                'label' => 'Weight',
                'name' => 'weight',
                'type' => 'number',
                'instructions' => 'Content elements with greater weight will be rendered first in "Content Elements" grid (Available from 3.7 version)',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
            array (
                'key' => 'field_531fecad3f103',
                'label' => 'Category',
                'name' => 'category',
                'type' => 'text',
                'instructions' => 'Category which best suites to describe functionality of this shortcode. Default categories: Content, Social, Structure. You can add your own category, simply enter new category title here',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fee393f104',
                'label' => 'Admin JS',
                'name' => 'admin_enqueue_js',
                'type' => 'text',
                'instructions' => 'Absolute url to javascript file, this js will be loaded in the js_composer edit mode (it allows you to add more functionality to your shortcode in js_composer edit mode)',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fee723f105',
                'label' => 'Admin CSS',
                'name' => 'admin_enqueue_css',
                'type' => 'text',
                'instructions' => 'Absolute url to css file if you need to add custom css for element block in js_composer constructor mode',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531fee8d3f106',
                'label' => 'Icon',
                'name' => 'icon',
                'type' => 'text',
                'instructions' => 'Css class with icon image. More info <a href="http://kb.wpbakery.com/index.php?title=VC_Shortcode_Icon_Class">here</a>',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531feef33f108',
                'label' => 'Element JS View',
                'name' => 'js_view',
                'type' => 'text',
                'instructions' => 'Set custom backbone.js view controller for this content element.',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_531feeac3f107',
                'label' => 'Custom Admin Markup',
                'name' => 'custom_markup',
                'type' => 'code_area',
                'instructions' => 'Custom html markup for representing shortcode in visual composer editor. You can use WordPress shortcodes.',
                'language' => 'htmlmixed',
                'theme' => 'monokai',
            ),
            array (
                'key' => 'field_53200e0c48741',
                'label' => 'Nested Elements',
                'name' => 'as_parent',
                'type' => 'radio',
                'instructions' => 'Allows elements to be nested inside this element.',
                'choices' => array (
                    'false' => 'False (not a parent element)',
                    'vc_row' => 'Row',
                ),
                'other_choice' => 1,
                'save_other_choice' => 1,
                'default_value' => 'false',
                'layout' => 'vertical',
            ),
            array (
                'key' => 'field_531fe8cb3e9e2',
                'label' => 'Element Fields',
                'name' => 'element_fields_json',
                'type' => 'code_area',
                'instructions' => 'This should be a JSON object that defines the fields of the element',
                'language' => 'javascript',
                'theme' => 'monokai',
            ),
            array (
                'key' => 'field_53227e6e147aa',
                'label' => 'Element Front End',
                'name' => '',
                'type' => 'message',
                'message' => '<h2>Frontend Element Options</h2>',
            ),
            array (
                'key' => 'field_53227d73adc83',
                'label' => 'Element Shortcode Markup',
                'name' => 'element_shortcode_markup',
                'type' => 'code_area',
                'instructions' => 
                    '<p>Do not use opening or closing PHP tags in this field.</p>
                    <p>The following fields are available:
                        <ul>
                            <li></li>
                        </ul>
                    </p>',
                'language' => 'php',
                'theme' => 'monokai',
            ),
            array (
                'key' => 'field_53227dcbadc84',
                'label' => 'Element Style',
                'name' => 'element_style',
                'type' => 'code_area',
                'instructions' => 'Write your custom CSS here.',
                'language' => 'css',
                'theme' => 'monokai',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'vcpp_element',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    );
    register_field_group($element_options_array);

}
