<?
/* Semantic Header
_______________________________________ */
vc_map( array(
	"name" => __("CTA Icon", "js_composer"),
	"base" => "cta-icon",
	"icon" => "icon-wpb-single-image",
	"category" => __('Content', 'js_composer'),
	"description" => __('Semantic Header', 'js_composer'),
	
	
	/* Get Javascript view linked up and working */
	// "js_view" => 'VcTextSemanticHeader',
	// "admin_enqueue_js" => array( plugins_url( '/js/vc-admin-extend.js', __FILE__ ) ),

		array(
			"type" => "textfield",
			"heading" => __("Main Header", "js_composer"),
			"param_name" => "cta-icon-main",
			'admin_label' => true,
			'class' => 'hdr-main',
			"description" => __(" ", "js_composer")
		),
		array(
			"type" => 'textfield',
			"heading" => __("Text Under HDR", "js_composer"),
			"param_name" => "cta-icon-text",
			"description" => __("", "js_composer"),
		),

		// Icon selector
		array(
			"type" => "icon",
			"class" => "",
			"heading" => __("Select Icon:", "icon-box"),
			"param_name" => "cta_icon",
			"admin_label" => true,
			"value" => "android",
			"description" => __("Select the icon from the list.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Size", "icon-box"),
			"param_name" => "size",
			"value" => 32,
			"min" => 16,
			"max" => 100,
			"suffix" => "px",
			"description" => __("Select the icon size.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Icon color - default or customize
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Icon Color:", "icon-box"),
			"param_name" => "color",
			"value" => array(
				"Use Default" => "",
				"Custom Color" => "color",
			),
			"description" => __("Select whether to use color for icon or not.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Customize Icon Color
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Select Icon Color:", "icon-box"),
			"param_name" => "icon_color",
			"value" => "#89BA49",
			"description" => __("Select the icon color.", "icon-box"),
			"dependency" => array(
				"element" => "color",
				"not_empty" => true,
			),
		),
		// Would you like icon border?
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Icon Border:", "icon-box"),
			"param_name" => "border",
			"value" => array(
				"No Border" => "",
				"Square Border" => "square",
				"Circle Border" => "circle",
			),
			"description" => __("Select if you want to display border around icon.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Give some spacing in icon and border
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Border Spacing", "icon-box"),
			"param_name" => "padding",
			"value" => 5,
			"min" => 0,
			"max" => 20,
			"suffix" => "px",
			"description" => __("Select spacing between icon and border.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Resize the border
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Border Width", "icon-box"),
			"param_name" => "width",
			"value" => "",
			"min" => 1,
			"max" => 10,
			"suffix" => "px",
			"description" => __("Select border width for icon.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Customize the border color
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Icon Border Color:", "icon-box"),
			"param_name" => "icon_border_color",
			"value" => "",
			"description" => __("Select the color for icon border.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Give some background to icon
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Icon Background Color:", "icon-box"),
			"param_name" => "icon_bg_color",
			"value" => "",
			"description" => __("Select the color for icon background.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		)
	)
);

class WPBakeryShortCode_ctaIcons extends WPBakeryShortCode {
	protected function content($atts, $content = null) {
		extract(shortcode_atts(array(
			'cta-icon-main' => '',
			'cta-icon-text' => '',
			'cta_icon' => '',

			// icon parameters
			'padding'	=> '',
			'icon_type'  => '',
			'icon_img'   => '',
			'icon'	   => 'android',
			'size'	   => '',
			'icon_color' => '',
			'color' 	  => '',
			'border'	 => '',
			'width' 	  => '',
			'icon_html_border_color' => '',
			'icon_bg_color' => '',

		), $atts));
		$css_class =  apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base'] );

		if ( $hdr_type == 'icon' ) $hdricon = '<i class="settings icon"></i>';

		$icon_html = '';
		$icon_prefix = '<div class="smicon-component '.$css_class.' '.$el_class.'">';
		$icon_suffix = '</div> <!-- smicon-component -->';
		$ex_class = '';
		
		if($pos != '')
			$ex_class .= ' icon-'.$pos;

		if($border != '')
			$ex_class .= ' icon-'.$border;

		$class = 'fa fa-'.$hdr_icon.' ';
		$border_width = ($width !== '') ? $width.'px' : ' 0px ';
		$style = ($color !== '') ? ' color:'.$icon_color.';' : ' ';
		$style .= ($size !== '') ? ' font-size:'.$size.'px;' : ' ';
		$style .= ($size !== '') ? ' width:'.$size.'px;' : ' ';
		$style .= ($size !== '') ? ' height:'.$size.'px;' : ' ';
		$style .= ($icon_border_color !== '') ? ' border: '.$border_width.' solid '.$icon_border_color.';' : ' ';
		$style .= ($icon_bg_color !== '') ? ' background: '.$icon_bg_color.';' : ' ';
		$style .= ($padding !== '') ? ' padding: '.$padding.'px;' : ' ';
		$icon_html .= '<i class="'.$class.'" style="'.$style.'"></i>';

		$icon_html = $icon_html;

		if ( $hdr_type == 'icon' ) $hdr_main_complete = '<'. $hdr_main_tag .' class="ui header '. $hdr_type .'" '. $hdr_main_style .'>'. $icon_html .' '. $hdr_main .'</'. $hdr_main_tag .'>';
		$output .= $hdr_main_complete;
		$output .= $hdr_sub_complete;

		$output = $this->startRow($el_position) . $output . $this->endRow($el_position);
		return $output;
    }
}
?>