<?
/* Semantic Header
_______________________________________ */
vc_map( array(
	"name" => __("Header", "js_composer"),
	"base" => "S_header",
	"icon" => "icon-wpb-single-image",
	"category" => __('Content', 'js_composer'),
	"description" => __('Semantic Header', 'js_composer'),
	
	
	/* Get Javascript view linked up and working */
	// "js_view" => 'VcTextSemanticHeader',
	// "admin_enqueue_js" => array( plugins_url( '/js/vc-admin-extend.js', __FILE__ ) ),

	"params" => array(
		array(
			"type" => 'dropdown',
			"heading" => __("Header Type", "js_composer"),
			"param_name" => "hdr_type",
			"description" => __("Only Check One", "js_composer"),
			"value" => Array(
				__("Basic", "js_composer") => 'basic',
				__("Descriptive", "js_composer") => 'desc', 
				__("Divider", "js_composer") => 'divider',
				__("Icon", "js_composer") => 'icon',
			)
		),
		array(
			"type" => 'dropdown',
			"heading" => __("Header Align", "js_composer"),
			"param_name" => "hdr_align",
			"description" => __("", "js_composer"),
			"value" => Array(
				__("Left", "js_composer") => 'left', 
				__("Center", "js_composer") => 'center',
				__("Right", "js_composer") => 'right'
			)
		),
		array(
			"type" => "textfield",
			"heading" => __("Main Header", "js_composer"),
			"param_name" => "hdr_main",
			'admin_label' => true,
			'class' => 'hdr-main',
			"description" => __(" ", "js_composer")
		),
		array(
			"type" => 'dropdown',
			"heading" => __("Main Header Tag", "js_composer"),
			"param_name" => "hdr_main_tag",
			"description" => __("", "js_composer"),
			"value" => Array(
				__("H1 (only use one per page)", "js_composer") => 'h1', 
				__("H2", "js_composer") => 'h2', 
				__("H3", "js_composer") => 'h3', 
				__("H4", "js_composer") => 'h4', 
				__("H5", "js_composer") => 'h5'
			)
		),
		array(
			"type" => 'textfield',
			"heading" => __("Main Header Margin", "js_composer"),
			"param_name" => "hdr_main_margin",
			"description" => __("", "js_composer"),
		),
		array(
			"type" => 'textfield',
			"heading" => __("Main Header Padding", "js_composer"),
			"param_name" => "hdr_main_padding",
			"description" => __("", "js_composer"),
		),

		// Sub header
		// ---------------
		array(
			"type" => "textfield",
			"heading" => __("Sub Header", "js_composer"),
			"param_name" => "hdr_sub",
			"description" => __("A descriptive secondary header.", "js_composer"),
			'admin_label' => true,
			'class' => 'hdr-sub',
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("desc")
			),
		),
		array(
			"type" => 'dropdown',
			"heading" => __("Sub Header Tag", "js_composer"),
			"param_name" => "hdr_sub_tag",
			"description" => __("", "js_composer"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("desc")
			),
			"value" => Array(
				__("H2", "js_composer") => 'h2', 
				__("H3", "js_composer") => 'h3', 
				__("H4", "js_composer") => 'h4', 
				__("H5", "js_composer") => 'h5',
				__("H6", "js_composer") => 'h6'
			)
		),

		// Icon selector
		array(
			"type" => "icon",
			"class" => "",
			"heading" => __("Select Icon:", "icon-box"),
			"param_name" => "hdr_icon",
			"admin_label" => true,
			"value" => "android",
			"description" => __("Select the icon from the list.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Size", "icon-box"),
			"param_name" => "size",
			"value" => 32,
			"min" => 16,
			"max" => 100,
			"suffix" => "px",
			"description" => __("Select the icon size.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Icon color - default or customize
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Icon Color:", "icon-box"),
			"param_name" => "color",
			"value" => array(
				"Use Default" => "",
				"Custom Color" => "color",
			),
			"description" => __("Select whether to use color for icon or not.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Customize Icon Color
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Select Icon Color:", "icon-box"),
			"param_name" => "icon_color",
			"value" => "#89BA49",
			"description" => __("Select the icon color.", "icon-box"),
			"dependency" => array(
				"element" => "color",
				"not_empty" => true,
			),
		),
		// Would you like icon border?
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Icon Border:", "icon-box"),
			"param_name" => "border",
			"value" => array(
				"No Border" => "",
				"Square Border" => "square",
				"Square Border With Background" => "square-solid",
				"Circle Border" => "circle",
				"Circle Border With Background" => "circle-solid",
			),
			"description" => __("Select if you want to display border around icon.", "icon-box"),
			"dependency" => array(
				"element" => "hdr_type", 
				"value" => array("icon")
			),
		),
		// Give some spacing in icon and border
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Border Spacing", "icon-box"),
			"param_name" => "padding",
			"value" => 5,
			"min" => 0,
			"max" => 20,
			"suffix" => "px",
			"description" => __("Select spacing between icon and border.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Resize the border
		array(
			"type" => "number",
			"class" => "",
			"heading" => __("Icon Border Width", "icon-box"),
			"param_name" => "width",
			"value" => "",
			"min" => 1,
			"max" => 10,
			"suffix" => "px",
			"description" => __("Select border width for icon.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Customize the border color
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Icon Border Color:", "icon-box"),
			"param_name" => "icon_border_color",
			"value" => "",
			"description" => __("Select the color for icon border.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		),
		// Give some background to icon
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading" => __("Icon Background Color:", "icon-box"),
			"param_name" => "icon_bg_color",
			"value" => "",
			"description" => __("Select the color for icon background.", "icon-box"),
			"dependency" => array(
				"element" => "border",
				"not_empty" => true,
			),
		)
	)
));

/*Semantic Header
_______________________________________ */

class WPBakeryShortCode_S_header extends WPBakeryShortCode {
	protected function content($atts, $content = null) {
		extract(shortcode_atts(array(
			'hdr_type' => '',
			'hdr_main' => '',
			'hdr_sub' => '',
			'hdr_align' => 'left',
			'hdr_main_tag' => '',
			'hdr_sub_tag' => '',
			'hdr_main_margin' => '',
			'hdr_main_padding' => '',
			'hdr_icon' => '',

			// icon parameters
			'padding'	=> '',
			'icon_type'  => '',
			'icon_img'   => '',
			'icon'	   => 'android',
			'size'	   => '',
			'icon_color' => '',
			'color' 	  => '',
			'border'	 => '',
			'width' 	  => '',
			'icon_html_border_color' => '',
			'icon_bg_color' => '',

		), $atts));
		$css_class =  apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base'] );

		$hdr_main_margin = ( $hdr_main_margin ? $hdr_main_margin = " margin-bottom: " . $hdr_main_margin . ";" : '' );
		$hdr_main_padding = ( $hdr_main_padding ? $hdr_main_padding = " padding-bottom: " . $hdr_main_padding . ";" : '' );
		$hdr_align = ( $hdr_align !== "left" ? $hdr_align = " text-align: " . $hdr_align . ";" : '' );

		if( $hdr_main_margin || $hdr_main_padding || $hdr_align ){
			$hdr_main_style = ' style="'. $hdr_align .''. $hdr_main_margin .''. $hdr_main_padding . '"';
		} else {
			$hdr_main_style = false;
		}

		if( $hdr_align ){
			$hdr_sub_style = ' style="'. $hdr_align .' margin:0px; padding:0px"';
		} else {
			$hdr_sub_style = false;
		}

		$hdr_main_complete = '<'. $hdr_main_tag .' class="ui header '. $hdr_type .'" '. $hdr_main_style .'>'. $hdr_main .'</'. $hdr_main_tag .'>';
        if ( $hdr_type == 'desc' ) $hdr_sub_complete = '<'. $hdr_sub_tag .' class="sub header" '.$hdr_sub_style.'>'. $hdr_sub .'</'. $hdr_sub_tag .'>';


		if ( $hdr_type == 'icon' ) $hdricon = '<i class="settings icon"></i>';

		$icon_html = '';
		$icon_prefix = '<div class="smicon-component '.$css_class.' '.$el_class.'">';
		$icon_suffix = '</div> <!-- smicon-component -->';
		$ex_class = '';
		
		if($pos != '')
			$ex_class .= ' icon-'.$pos;

		if($border != '')
			$ex_class .= ' icon-'.$border;

		$class = 'fa fa-'.$hdr_icon.' ';
		$border_width = ($width !== '') ? $width.'px' : ' 0px ';
		$style = ($color !== '') ? ' color:'.$icon_color.';' : ' ';
		$style .= ($size !== '') ? ' font-size:'.$size.'px;' : ' ';
		$style .= ($size !== '') ? ' width:'.$size.'px;' : ' ';
		$style .= ($size !== '') ? ' height:'.$size.'px;' : ' ';
		$style .= ($icon_border_color !== '') ? ' border: '.$border_width.' solid '.$icon_border_color.';' : ' ';
		$style .= ($icon_bg_color !== '') ? ' background: '.$icon_bg_color.';' : ' ';
		$style .= ($padding !== '') ? ' padding: '.$padding.'px;' : ' ';
		$icon_html .= '<i class="'.$class.'" style="'.$style.'"></i>';

		$icon_html = $icon_html;

		if ( $hdr_type == 'icon' ) $hdr_main_complete = '<'. $hdr_main_tag .' class="ui header '. $hdr_type .'" '. $hdr_main_style .'>'. $icon_html .' '. $hdr_main .'</'. $hdr_main_tag .'>';
		$output .= $hdr_main_complete;
		$output .= $hdr_sub_complete;

		$output = $this->startRow($el_position) . $output . $this->endRow($el_position);
		return $output;
    }
}
?>