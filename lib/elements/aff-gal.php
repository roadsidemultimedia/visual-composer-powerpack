<?
/* AFF Gallery Holder
_________________________________________ */
vc_map( array(
    "name" => __("Affiliate Holder", "js_composer"),
    "base" => "aff_gal_holder",
    "icon" => "icon-wpb-box",
    "as_parent" => array('only' => 'aff_img,vc_single_image'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("How Many Per Row?", "js_composer"),
            "param_name" => "owl_row",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Slide Speed", "js_composer"),
            "param_name" => "owl_speed",
            "description" => __(" Speed in Miliseconds: 3000", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        )
    ),
    "js_view" => 'VcColumnView'
) );
/* AFF IMG
_______________________________________ */
vc_map( array(
    "name" => __("AFF Image", "js_composer"),
    "base" => "aff_img",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "as_child" => array('only' => 'aff_gal_holder'),
    "params" => array(
        // add params same as with any other content element
		array(
            "type" => "attach_image",
            "heading" => __("Image", "js_composer"),
            "param_name" => "image",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),
    )
) );
/* AFF IMG
_______________________________________ */

add_shortcode( 'aff_img', 'affiliate_image' );

function affiliate_image($atts, $content = null) {

        extract(shortcode_atts(array(
             'title' => '',
    'image' => $image,
    'img_size'  => 'full',
    'img_link_large' => false,
    'img_link' => '',
    'img_link_target' => '_self',
    'alignment' => 'left',
    'el_class' => '',
    'css_animation' => ''
), $atts));

$img_id = preg_replace('/[^\d]/', '', $image);
$img = wpb_getImageBySize(array( 'attach_id' => $img_id, 'thumb_size' => $img_size ));
if ( $img == NULL ) $img['full'] = '<img src="http://placekitten.com/g/400/300" /> <small>'.__('This is image placeholder, edit your page to replace it.', 'js_composer').'</small>';

$image_string = !empty($link_to) ? '<a'.$a_class.' href="'.$link_to.'"'.($img_link_target!='_self' ? ' target="'.$img_link_target.'"' : '').'>'.$img['thumbnail'].'</a>' : $img['thumbnail'];

$output .= "\n\t\t".'<div class="owlitem">';
$output .= "\n\t\t\t".$image_string;
$output .= "\n\t\t".'</div> ';
        return $output;
    }
/*Box Holder
_______________________________________ */
class WPBakeryShortCode_aff_gal_holder extends WPBakeryShortCodesContainer {
    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'el_class' => '',
            'owl_row' => '5',
            'owl_speed' => '3000',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        $output  = '<div id="owl">';
            $output .= wpb_js_remove_wpautop($content, true);
        $output .= '</div>';
        $output .= "\n\t\t".'<script>jQuery(function($){';
        $output .= "\n\t\t".'$(document).ready(function() {';
        $output .= "\n\t\t".'$("#owl").owlCarousel({';
        $output .= "\n\t\t".'autoPlay: '. $owl_speed .',';
        $output .= "\n\t\t".'items : '. $owl_row .',';
        $output .= "\n\t\t".'itemsDesktop : [1199,3],';
        $output .= "\n\t\t".'itemsDesktopSmall : [979,3],';
        $output .= "\n\t\t".'itemsTablet : [768,2],';
        $output .= "\n\t\t".'itemsMobile : [479,1],';
        $output .= "\n\t\t".'stopOnHover : true';
        $output .= "\n\t\t".'});';
        $output .= "\n\t\t".'});'; 
        $output .= "\n\t\t".'});</script>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

?>