<?
/* Semantic Animated Button 
_______________________________________ */
vc_map( array(
    "name" => __("Animated Button", "js_composer"),
    "base" => "S_anim_button",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
		array(
			"type" => "dropdown",
			"heading" => __("Size of button", "js_composer"),
			"param_name" => "s_anim_button_size",
			"value" => array('Huge' => 'huge', 'Large' => 'large', 'Small' => 'small', 'tiny' => 'tiny'),
			"description" => __(".", "js_composer")
		),
		array(
            "type" => "textfield",
            "heading" => __("Visible", "js_composer"),
            "param_name" => "s_anim_button_visible",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Roll Over", "js_composer"),
            "param_name" => "s_anim_button_hidden",
            "description" => __("", "js_composer")
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Color of button", "js_composer"),
			"param_name" => "s_anim_button_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("Link for Button", "js_composer"),
			"param_name" => "s_anim_button_link",
			"description" => __("Choose a color any color", "js_composer")
		)
    ),
    "js_view" => 'VcButtonView'
) );
/*Semantic Animated
_______________________________________ */
class WPBakeryShortCode_S_anim_button extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			's_anim_button_size' => '',
			's_anim_button_visible' => '',
			's_anim_button_hidden' => '',
			's_anim_button_link' => '',
			's_anim_button_color' => '',
			
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
	        $output = '<div class="ui animated fade button">';
	        $output .= '<div class="visible content"> ' . $s_anim_button_visible . '</div>';
	        $output .= '<div class="hidden content">';
	        $output .= $s_anim_button_hidden;
	        $output .= '</div></div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/* Semantic "or" Button 
_______________________________________ */
vc_map( array(
    "name" => __("Semantic -or- Button", "js_composer"),
    "base" => "S_or_button",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
		array(
			"type" => "dropdown",
			"heading" => __("Size of button", "js_composer"),
			"param_name" => "s_or_button_size",
			"value" => array('Huge' => 'huge', 'Large' => 'large', 'Small' => 'small', 'tiny' => 'tiny'),
			"description" => __("The respective size of your box. 1 thru 5.", "js_composer")
		),
		array(
            "type" => "textfield",
            "heading" => __("Left Side", "js_composer"),
            "param_name" => "s_or_button_left",
            "description" => __("Text for the left side of the box.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Right Side", "js_composer"),
            "param_name" => "s_or_button_right",
            "description" => __("Text for the right side of the box.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Button Width", "js_composer"),
            "param_name" => "s_or_button_width",
            "description" => __("To align the button to the center a width must be entered.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Left Button Width", "js_composer"),
            "param_name" => "s_or_button_left_width",
            "description" => __("", "js_composer")
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Color of Left button", "js_composer"),
			"param_name" => "s_or_button_left_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		),
		array(
            "type" => "textfield",
            "heading" => __("Right Button Width", "js_composer"),
            "param_name" => "s_or_button_right_width",
            "description" => __("", "js_composer")
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Color of Right button", "js_composer"),
			"param_name" => "s_or_button_right_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		)
    ),
    "js_view" => 'VcButtonView'
) );
/*Semantic OR Button
_______________________________________ */
class WPBakeryShortCode_S_or_button extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			's_or_button_size' => '',
			's_or_button_left' => '',
			's_or_button_left_color' => '',
			's_or_button_right' => '',
			's_or_button_right_color' => '',
			's_or_button_width' => '',
			's_or_button_left_width' => '',
			's_or_button_right_width' => '',
			
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
	        $output = '<div style="margin:auto; width:'. $s_or_button_width .'"><div class="ui ' . $s_or_button_size . ' buttons" style="margin-top:20px">';
	        $output .= '<div class="ui ' . $s_or_button_left_color . ' button" style="width:'. $s_or_button_left_width .'"> ' . $s_or_button_left . '</div>';
	        $output .= '<div class="or"></div>';
	        $output .= '<div class="ui ' . $s_or_button_right_color . ' button" style="width:'. $s_or_button_right_width .'"> ' . $s_or_button_right . '</div>';
        $output .= '</div></div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/* Semantic Descriptive Button 
_______________________________________ */
vc_map( array(
    "name" => __("Descriptive Button", "js_composer"),
    "base" => "S_descriptive_button",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
		array(
			"type" => "dropdown",
			"heading" => __("Size of button", "js_composer"),
			"param_name" => "s_descriptive_button_size",
			"value" => array('Huge' => 'huge', 'Large' => 'large', 'Small' => 'small', 'tiny' => 'tiny'),
			"description" => __(".", "js_composer")
		),
		array(
            "type" => "textfield",
            "heading" => __("Main Text", "js_composer"),
            "param_name" => "s_descriptive_button_main",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Descriptive Text", "js_composer"),
            "param_name" => "s_descriptive_button_desc",
            "description" => __("", "js_composer")
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Color of button", "js_composer"),
			"param_name" => "s_descriptive_button_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("Link for Button", "js_composer"),
			"param_name" => "s_descriptive_button_link",
			"description" => __("", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("Width", "js_composer"),
			"param_name" => "s_descriptive_button_width",
			"description" => __("", "js_composer")
		),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        )
    ),
  "custom_markup" => '  
  <div class="wpb_tabs_holder wpb_holder clearfix vc_container_for_children">
  <ul class="tabs_controls">
  </ul>
  %content%
  </div>',
  "js_view" => ($vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35')
) );
/*Semantic Animated
_______________________________________ */
class WPBakeryShortCode_S_descriptive_button extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			's_descriptive_button_size' => '',
			's_descriptive_button_main' => '',
			's_descriptive_button_desc' => '',
			's_descriptive_button_width' => '',
			's_descriptive_button_link' => '',
			's_descriptive_button_color' => '',
			'el_class' => '',
			
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
	        $output .= '<div class="ui circular '. $s_descriptive_button_color .' '. $s_descriptive_button_size .' '. $el_class .' button" style="margin: 20px auto 20px auto; width:'. $s_descriptive_button_width .'; display:inherit;"> ' . $s_descriptive_button_main . '';
	        $output .= '<div class="descriptive_button" style="font-size:10px;">'. $s_descriptive_button_desc .'</div>';
	        $output .= '</div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        $content = $output;
        return $content;
    }
}
?>