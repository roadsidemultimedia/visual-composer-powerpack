<?
/* Semantic Animated Button 
_______________________________________ */
vc_map( array(
    "name" => __("Custom Gmap", "js_composer"),
    "base" => "custom_gmap",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
		array(
            "type" => "textfield",
            "heading" => __("Map Title", "js_composer"),
            "param_name" => "custom_gmap_title",
            "description" => __("", "js_composer")
        ),
		array(
            "type" => "textfield",
            "heading" => __("Latitude", "js_composer"),
            "param_name" => "custom_gmap_lat",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Longitude", "js_composer"),
            "param_name" => "custom_gmap_long",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Zoom", "js_composer"),
            "param_name" => "custom_gmap_zoom",
            "description" => __("Zoom level", "js_composer")
        ),
		array(
            "type" => "textfield",
            "heading" => __("Icon Link", "js_composer"),
            "param_name" => "custom_gmap_icon",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Width", "js_composer"),
            "param_name" => "custom_gmap_width",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Height", "js_composer"),
            "param_name" => "custom_gmap_height",
            "description" => __("", "js_composer")
        ),
    ),
    "js_view" => 'VcButtonView'
) );
/*Semantic Animated
_______________________________________ */
class WPBakeryShortCode_custom_gmap extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			'custom_gmap_zoom' => '',
			'custom_gmap_lat' => '',
			'custom_gmap_long' => '',
			'custom_gmap_icon' => '',
			'custom_gmap_width' => '',
			'custom_gmap_height' => '',
			'custom_gmap_title' => '',
			
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
	        $output = '<script type="text/javascript">';
			$output .= "\n".'function cgmap() {';
			$output .= "\n".'	var latlng = new google.maps.LatLng('. $custom_gmap_lat .', '. $custom_gmap_long .');';
			$output .= "\n".'	var settings = {';
			$output .= "\n".'		zoom: '. $custom_gmap_zoom .',';
			$output .= "\n".'		center: latlng,';
			$output .= "\n".'		mapTypeControl: true,';
			$output .= "\n".'		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},';
			$output .= "\n".'		navigationControl: true,';
			$output .= "\n".'		navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},';
			$output .= "\n".'		mapTypeId: google.maps.MapTypeId.ROADMAP};';
			$output .= "\n".'	var map = new google.maps.Map(document.getElementById("custom_gmap"), settings);';
			$output .= "\n".'';
			$output .= "\n"."	var companyImage = new google.maps.MarkerImage('". $custom_gmap_icon ."',";
			$output .= "\n".'		new google.maps.Size(100,50),';
			$output .= "\n".'		new google.maps.Point(0,0),';
			$output .= "\n".'		new google.maps.Point(50,50)';
			$output .= "\n".'	);';
			$output .= "\n"."	var companyShadow = new google.maps.MarkerImage('images/logo_shadow.png',";
			$output .= "\n".'		new google.maps.Size(130,50),';
			$output .= "\n".'		new google.maps.Point(0,0),';
			$output .= "\n".'		new google.maps.Point(65, 50));';
			$output .= "\n".'	var companyPos = new google.maps.LatLng('. $custom_gmap_lat .', '. $custom_gmap_long .');';
			$output .= "\n".'	var companyMarker = new google.maps.Marker({';
			$output .= "\n".'		position: companyPos,';
			$output .= "\n".'		map: map,';
			$output .= "\n".'		icon: companyImage,';
			$output .= "\n".'		shadow: companyShadow,';
			$output .= "\n".'		title:"Roadside",';
			$output .= "\n".'		zIndex: 3});';
			$output .= "\n"."					google.maps.event.addListener(companyMarker, 'click', function() {";
			$output .= "\n".'		infowindow.open(map,companyMarker);';
			$output .= "\n".'	});';
			$output .= "\n".'} window.onload = cgmap;';
		    $output .= "\n".'</script>';
		    $output .= "\n".'<div id="custom_gmap" style="width:'. $custom_gmap_width .'; height:'. $custom_gmap_height .'"></div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

?>