<?php
/*
Embeddable Parallax Backgrounds for VC
Description: Adds new options to Visual Composer rows to enable parallax scrolling to row background images.
Author: Benjamin Intal, Gambit
Version: 1.5
Author URI: http://gambit.ph
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Change this constant to the text domain your theme / plugin is using
defined( 'VCPP_TEXT_DOMAIN' ) or define( 'VCPP_TEXT_DOMAIN', 'default' );


if ( ! class_exists( 'rsmm_parallax_backgrounds' ) ) {

	/**
	 * Parallax Background Class
	 *
	 * @since	1.0
	 */
	class rsmm_parallax_backgrounds {


		/**
		 * Constructor, checks for Visual Composer and defines hooks
		 *
		 * @return	void
		 * @since	1.0
		 */
		function __construct() {
			// Check if Visual Composer is installed
			if ( ! defined( 'WPB_VC_VERSION' ) || ! function_exists( 'vc_add_param' ) ) {
				return;
			}

			add_action( 'wp_head', array( $this, 'initParallax' ) );
			add_filter( 'gambit_add_parallax_div', array( __CLASS__, 'createParallaxDiv' ), 10, 3 );

			$this->addParallaxParams();
		}


		/**
		 * Initializes the parallax background in the front end
		 *
		 * @return	void
		 * @since	1.0
		 */
		public function initParallax() {
			?>
			<script>
			jQuery(document).ready(function($) {
				"use strict";

				// Initialize Rowspans
				$('div.bg-parallax').each(function() {
					if ( typeof $(this).attr('data-row-span') == 'undefined' ) {
						return;
					}
					var rowSpan = parseInt( $(this).attr('data-row-span') );
					if ( isNaN( rowSpan ) ) {
						return;
					}
					if ( rowSpan == 0 ) {
						return;
					}

					var $nextRows = $(this).nextAll('.wpb_row');
					$nextRows.splice(0,1);
					$nextRows.splice(rowSpan);

					// Clear stylings for the next rows that our parallax will occupy
					$nextRows.each(function() {
						if ( $(this).prev().is('.bg-parallax') ) {
							$(this).prev().remove();
						}
						// we need to apply this class to make the row children visible
						$(this).addClass('bg-parallax-parent')
						// we need to clear the row background to make the parallax visible
						.css( {
							'backgroundImage': '',
							'backgroundColor': 'transparent'
						} );
					});
				})

				// Initialize parallax
				$('div.bg-parallax').each(function() {
					var $row = $(this).next();
					if ( $row.css( 'backgroundSize' ) == 'contain' ) {
						$row.css( 'backgroundSize', 'cover' );
					}
					$(this).css( {
						'backgroundImage': $row.css( 'backgroundImage' ),
						'backgroundRepeat': $row.css( 'backgroundRepeat' ),
						'backgroundSize': $row.css( 'backgroundSize' )
					} )
					.prependTo( $row.addClass('bg-parallax-parent') )
					.scrolly2().trigger('scroll');
					$row.css( {
						'backgroundImage': '',
						'backgroundRepeat': '',
						'backgroundSize': ''
					});

					if ( $(this).attr('data-direction') == 'up' || $(this).attr('data-direction') == 'down' ) {
						$(this).css( 'backgroundAttachment', 'fixed' );
					}
				});

				$(window).resize( function() {
					// Break container
					$('div.bg-parallax, div.full-width').each(function() {
						if ( typeof $(this).attr('data-full-width') == 'undefined' ) {
							return;
						}
						var fullWidth = $(this).attr('data-full-width');
						if ( !fullWidth == true ) {
							return;
						}

						var $immediateParent = $(this).parent();
						var $superParent = $('html');

						// Compute dimensions & location
						var parentWidth = $superParent.width() + parseInt( $superParent.css('paddingLeft') ) + parseInt( $superParent.css('paddingRight') );
						var left = - ( $immediateParent.offset().left - $superParent.offset().left );
						if ( left > 0 ) {
							left = 0;
						}

						$(this).addClass('broke-out broke-out-child')
						.css({
							'width': parentWidth,
							'left': left
						})
						.parent().addClass('broke-out broke-out-parent');
					});

					// span multiple rows
					$('div.bg-parallax').each(function() {
						if ( typeof $(this).attr('data-row-span') == 'undefined' ) {
							return;
						}
						var rowSpan = parseInt( $(this).attr('data-row-span') );
						if ( isNaN( rowSpan ) ) {
							return;
						}
						if ( rowSpan == 0 ) {
							return;
						}

						var $nextRows = $(this).parent('.wpb_row').nextAll('.wpb_row');
						$nextRows.splice(rowSpan);

						// Clear stylings for the next rows that our parallax will occupy
						var heightToAdd = 0;
						$nextRows.each(function() {
							heightToAdd += $(this).height()
								+ parseInt($(this).css('paddingTop'))
								+ parseInt($(this).css('paddingBottom'));
						});
						$(this).css( 'height', 'calc(100% + ' + heightToAdd + 'px)' );
					});
				});

				$(window).trigger('resize');
			});
			</script>
			<style>
			/* Parallax mandatory styles */
			.bg-parallax {
				width: 100%;
				height: 100%;
				position: absolute;
				display: block;
				top: 0;
				left: 0;
				z-index: 0;
			}
			.bg-parallax-parent {
				position: relative;
			}
			.bg-parallax-parent > *:not(.bg-parallax) {
				position: relative;
				z-index: 1;
			}
			.bg-parallax-parent.broke-out {
				overflow: visible;
			}
			@media (max-width: 980px) {
				/* Disable parallax for mobile devices */
				.bg-parallax[data-mobile-enabled=""] {
					background-position: 50% 50% !important;
				}
			}
			</style>
			<?php
		}


		/**
		 * Creates the placeholder for the row with the parallax bg
		 *
		 * @param	string $output An empty string
		 * @param	array $atts The attributes of the vc_row shortcode
		 * @param	string $content The contents of vc_row
		 * @return	string The placeholder div
		 * @since	1.0
		 */
		public static function createParallaxDiv( $output, $atts, $content ) {
			extract( shortcode_atts( array(
				'parallax'		=> '',
				'speed'			=> '',
				'enable_mobile'	=> '',
				'break_parents'	=> '',
				'full_width'	=> 'false',
				'row_span'		=> '',
				'bg_color'		=> '',
				'overlay_color' => '',
				'opacity' => '',
				'border_top_color' =>'#000',
				'border_top_width' =>'',
				'border_bottom_color' =>'#000',
				'border_bottom_width' =>'',
			), $atts ) );

			// echo "<pre>" . print_r($atts, true) . "</pre>";

			//Borders
			$border_top = "";
			$border_bottom = "";
			if ($border_top_width) $border_top = 'border-top:'.$border_top_width.' solid '.$border_top_color.';';
			if ($border_bottom_width) $border_bottom = 'border-bottom:'.$border_bottom_width.' solid '.$border_bottom_color.';';

			//Overlay
			$overlay_style = "";
			$overlay_rgb = vcpp_HexToRGB($overlay_color);
			$overlay_rgba = $overlay_rgb["r"].",".$overlay_rgb["g"].",".$overlay_rgb["b"].",";
			if ($overlay_color) {
				if( !$opacity ) $opacity = 1;
				$overlay_style = 'background-color:rgba('.$overlay_rgba.$opacity.');';
			}

			//Background Style
			$bg_color_style = 'background-color:'.$bg_color.';';

			if ( empty( $parallax ) ) {
				// return "";
			}

			wp_enqueue_script( 'jquery-scrolly', VCPP_URL . '/lib/assets/js/jquery.scrolly.js', array( 'jquery' ), null, true );

			$parallaxClass = ( $parallax == "none" ) ? "" : "bg-parallax";
			$parallaxClass = ( $break_parents == "none" ) ? "" : "bg-parallax";
			$parallaxClass = in_array( $parallax, array( "none", "up", "down", "left", "right", "bg-parallax" ) ) ? $parallaxClass : "";

				
			if ( ! $parallaxClass ) {
				return '';
			} 
			// echo "<pre>parallaxClass: <br>" . print_r($parallaxClass, true) . "</pre>";

			$output = "<div style='".$border_top.$border_bottom.$bg_color_style."' class='" . esc_attr( $parallaxClass ) . $break_parents_class . "' data-direction='" . esc_attr( $parallax ) . "' data-velocity='" . esc_attr( (float)$speed * -1 ) . "' data-mobile-enabled='" . esc_attr( $enable_mobile ) . "' data-full-width='" . esc_attr( $full_width ) . "' data-row-span='" . esc_attr( $row_span ) . "'>";
			
			if ($overlay_color) $output .= '<div class="bgwithparallax_overlay" style="'.$overlay_style.'"></div>';
			
			$output .= "</div>";

			return $output;
		}


		/**
		 * Adds the parameter fields to the VC row
		 *
		 * @return	void
		 * @since	1.0
		 */
		private function addParallaxParams() {

			vc_remove_param('vc_row', 'padding');
			vc_remove_param('vc_row', 'el_class');
			vc_remove_param('vc_row', 'bg_color');
			vc_remove_param('vc_row', 'bg_image');
			vc_remove_param('vc_row', 'bg_image_repeat');
			vc_remove_param('vc_row', 'margin_bottom');
			vc_remove_param('vc_row', 'font_color');


			$setting = array(
				"type" => "colorpicker",
				"heading" => __('Font Color', 'wpb'),
				"param_name" => "font_color",
				"description" => __("Select font color", "wpb"),
				"edit_field_class" => 'col-md-6'
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "colorpicker",
				"heading" => __("Custom Background Color", "wpb"),
				"param_name" => "bg_color",
				"description" => __("Select backgound color for your row", "wpb"),
				"edit_field_class" => 'col-md-6'
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "attach_image",
				"heading" => __('Background Image', 'wpb'),
				"param_name" => "bg_image",
				"description" => __("Select background image for your row", "wpb")
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "dropdown",
				"heading" => __('Background Repeat', 'wpb'),
				"param_name" => "bg_image_repeat",
				"value" => array(
					__("Default", 'wpb') => 'cover',
					__("Cover", 'wpb') => 'cover',
					__('Contain', 'wpb') => 'contain',
					__('Repeat X (horizontally)', 'wpb') => 'repeat-x',
					__('Repeat Y (vertically)', 'wpb') => 'repeat-y',
					__('No Repeat', 'wpb') => 'no-repeat',
				),
				"description" => __("Select how a background image will be repeated", "wpb"),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true)
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "colorpicker",
				"holder" => "div",
				"heading" => __("Overlay Color", VCPP_TEXT_DOMAIN),
				"param_name" => "overlay_color",
				"description" => __("Background Color Overlay color. This is a color that overlays the background image.", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "textfield",
				"heading" => __("Opacity", VCPP_TEXT_DOMAIN),
				"param_name" => "opacity",
				"value" => "0.5",
				"description" => __("Opacity of Background Color Overlay. The value should be betweeon 0.1 and 1.0", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "overlay_color", 'not_empty' => true)
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( "Background Image Parallax", VCPP_TEXT_DOMAIN ),
				"param_name" => "parallax",
				"value" => array(
					"No Parallax" => "none",
					"Up" => "up",
					"Down" => "down",
					"Left" => "left",
					"Right" => "right",
				),
				"description" => __("Select the parallax effect for your background image. You must have a background image to use this.", VCPP_TEXT_DOMAIN ),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true)
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __( "Parallax Speed", VCPP_TEXT_DOMAIN ),
				"param_name" => "speed",
				"value" => "0.3",
				"description" => __( "The movement speed, value should be between 0.1 and 1.0", VCPP_TEXT_DOMAIN ),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "checkbox",
				"heading" => __("Force Full Width Background?", "js_composer"),
				"param_name" => "full_width",
				"description" => __("Should the background expand beyond it's container to display at the full width of the window?", "js_composer"),
				"value" => Array(__("Yes", "js_composer") => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( "Breakout Background Horizontally", VCPP_TEXT_DOMAIN ),
				"param_name" => "break_parents",
				"value" => array(
					"Don't break out the row container" => "0",
					sprintf( _n( "Break out of 1 container", "Break out of %d containers", 1, VCPP_TEXT_DOMAIN ), 1 ) => "1",
					sprintf( _n( "Break out of 1 container", "Break out of %d containers", 2, VCPP_TEXT_DOMAIN ), 2 ) => "2",
					sprintf( _n( "Break out of 1 container", "Break out of %d containers", 3, VCPP_TEXT_DOMAIN ), 3 ) => "3",
					sprintf( _n( "Break out of 1 container", "Break out of %d containers", 4, VCPP_TEXT_DOMAIN ), 4 ) => "4",
					sprintf( _n( "Break out of 1 container", "Break out of %d containers", 5, VCPP_TEXT_DOMAIN ), 5 ) => "5",
					__( "Break out of all containers (full page width)", VCPP_TEXT_DOMAIN ) => "99",
				),
				"description" => __( "Adjust this option to let the parallax effect stretch outside it's current container and occupy the full window width.", VCPP_TEXT_DOMAIN ),
			);
			vc_add_param( 'vc_row', $setting );

			$setting = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( "Breakout Background Vertically", VCPP_TEXT_DOMAIN ),
				"param_name" => "row_span",
				"value" => array(
					"Occupy this row only" => "0",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 1, VCPP_TEXT_DOMAIN ), 1 ) => "1",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 2, VCPP_TEXT_DOMAIN ), 2 ) => "2",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 3, VCPP_TEXT_DOMAIN ), 3 ) => "3",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 4, VCPP_TEXT_DOMAIN ), 4 ) => "4",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 5, VCPP_TEXT_DOMAIN ), 5 ) => "5",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 6, VCPP_TEXT_DOMAIN ), 6 ) => "6",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 7, VCPP_TEXT_DOMAIN ), 7 ) => "7",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 8, VCPP_TEXT_DOMAIN ), 8 ) => "8",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 9, VCPP_TEXT_DOMAIN ), 9 ) => "9",
					sprintf( _n( "Occupy also the next row", "Occupy also the next %d rows", 10, VCPP_TEXT_DOMAIN ), 10 ) => "10",
				),
				"description" => __( "This allows the background to also span across subsequent Visual Composer rows.", VCPP_TEXT_DOMAIN ),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "textfield",
				"heading" => __('Padding', 'wpb'),
				"param_name" => "padding",
				"description" => __("<p>You can use px, em, %, etc. or enter just number and it will use pixels.<br>If you want to enter seperate padding values for each side, you can use shorthand<br>TOP -> RIGHT -> BOTTOM -> LEFT or more easily remembered as <strong>TR</strong>ou<strong>BL</strong>e | Example: <strong>100px 20px 100px 20px</strong> = 100px top and bottom padding, and 20px left and right padding.<br>TOP, BOTTOM -> LEFT, RIGHT | Example: <strong>100px 0</strong> = 100px top and bottom padding, and 0 left and right padding.</p>", VCPP_TEXT_DOMAIN),
				"edit_field_class" => 'col-md-6',
			);
			vc_add_param( 'vc_row', $setting );

			$setting = array(
				"type" => "textfield",
				"heading" => __('Bottom margin', 'wpb'),
				"param_name" => "margin_bottom",
				"description" => __("You can use px, em, %, etc. or enter just number and it will use pixels. ", "wpb"),
				"edit_field_class" => 'col-md-6'
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "checkbox",
				"heading" => __("Custom Borders?", "js_composer"),
				"param_name" => "custom_borders",
				"description" => __("Would you like to customize the borders?", "js_composer"),
				"value" => Array(__("Yes", "js_composer") => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );

			$setting = array(
				"type" => "textfield",
				"heading" => __("Border Top Width", VCPP_TEXT_DOMAIN),
				"param_name" => "border_top_width",
				"value" => "0",
				"description" => __("Set Top Border Size of the Container", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "custom_borders", 'value' => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Border Top Color", VCPP_TEXT_DOMAIN),
				"param_name" => "border_top_color",
				"value" => '#44576a',
				"description" => __("Choose the Top Border Color of the Container", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "custom_borders", 'value' => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "textfield",
				"heading" => __("Border Bottom Width", VCPP_TEXT_DOMAIN),
				"param_name" => "border_bottom_width",
				"value" => "0",
				"description" => __("Set Bottom Border Size of the Container", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "custom_borders", 'value' => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Border Bottom Color", VCPP_TEXT_DOMAIN),
				"param_name" => "border_bottom_color",
				"value" => '#44576a',
				"description" => __("Choose the Bottom Border Color of the Container", VCPP_TEXT_DOMAIN),
				"dependency" => Array('element' => "custom_borders", 'value' => 'yes'),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "checkbox",
				"class" => "",
				"param_name" => "enable_mobile",
				"value" => array( __( "Check this to enable the parallax effect in mobile devices", VCPP_TEXT_DOMAIN ) => "parallax-enable-mobile" ),
				"description" => __( "Parallax effects would most probably cause slowdowns when your site is viewed in mobile devices. If the device width is less than 980 pixels, then it is assumed that the site is being viewed in a mobile device.", VCPP_TEXT_DOMAIN ),
				"dependency" => Array('element' => "bg_image", 'not_empty' => true),
			);
			vc_add_param( 'vc_row', $setting );


			$setting = array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			);
			vc_add_param( 'vc_row', $setting );
		}
	}


	new rsmm_parallax_backgrounds();
}



if ( ! function_exists( 'vc_theme_before_vc_row' ) ) {


	/**
	 * Adds the placeholder div right before the vc_row is printed
	 *
	 * @param	array $atts The attributes of the vc_row shortcode
	 * @param	string $content The contents of vc_row
	 * @return	string The placeholder div
	 * @since	1.0
	 */
	function vc_theme_before_vc_row($atts, $content = null) {
		return apply_filters( 'gambit_add_parallax_div', '', $atts, $content );
	}
}

/* ------------------------------------- */
/* Color Hex to RGB
/* ------------------------------------- */
function vcpp_HexToRGB($hex) {
	$hex = str_replace("#", "", $hex);
	$color = array();

	if(strlen($hex) == 3) {
		$color['r'] = hexdec(substr($hex, 0, 1) . $r);
		$color['g'] = hexdec(substr($hex, 1, 1) . $g);
		$color['b'] = hexdec(substr($hex, 2, 1) . $b);
	}
	else if(strlen($hex) == 6) {
		$color['r'] = hexdec(substr($hex, 0, 2));
		$color['g'] = hexdec(substr($hex, 2, 2));
		$color['b'] = hexdec(substr($hex, 4, 2));
	}

	return $color;
}