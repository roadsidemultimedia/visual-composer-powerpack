<?
/* Fancy Lists By Curtis Grant
---------------------------------------------------------- */
vc_map( array(
    "name"		=> __("Fancy Lists", "js_composer"),
	"base"		=> "fancy_lists_item",
    "class"		=> "",
    "icon" => "icon-wpb-fancylist",
	"content_element" => true,
    "params"	=> array(
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Text", "js_composer"),
            "param_name" => "flitem",
            "value" => __(" ", "js_composer"),
            "description" => __("Enter your content.", "js_composer")
        ),
		// Play with icon selector
							// Play with icon selector
							array(
								"type" => "icon",
								"class" => "",
								"heading" => __("Select Icon:", "icon-box"),
								"param_name" => "icon",
								"admin_label" => true,
								"value" => "android",
								"description" => __("Select the icon from the list.", "icon-box"),
								"dependency" => Array("element" => "icon_type","value" => array("font-awesome")),
							),
							// Resize the icon
							array(
								"type" => "number",
								"class" => "",
								"heading" => __("Icon Size", "icon-box"),
								"param_name" => "size",
								"value" => 12,
								"min" => 10,
								"max" => 100,
								"suffix" => "px",
								"description" => __("Select the icon size.", "icon-box")
							),
							// Customize Icon Color
							array(
								"type" => "colorpicker",
								"class" => "",
								"heading" => __("Select Icon Color:", "icon-box"),
								"param_name" => "icon_color",
								"value" => "#89BA49",
								"description" => __("Select the icon color.", "icon-box"),
								"dependency" => array(
												"element" => "color",
												"not_empty" => true,
											),
								"dependency" => Array("element" => "icon_type","value" => array("font-awesome")),
							),
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        ),
    )
) );

/*Fancy List Container
_______________________________________ */
vc_map( array(
    "name" => __("Fancy List Holder", "js_composer"),
    "base" => "fancy_list_holder",
	"icon" => "icon-wpb-fancylist",
    "as_parent" => array('only' => 'fancy_lists_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Left Margin", "js_composer"),
            "param_name" => "fl_list_left_margin",
            "description" => __("Add only the number", "js_composer")
        ),
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        )
    ),
    "js_view" => 'VcColumnView'
) );
/*Fancy List Item Shortcode
_______________________________________ */
class WPBakeryShortCode_fancy_lists_item extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'icon' => 'none',
            'el_class' => '',
            'flitem' => '',
			'size' => '',
			'icon_color' => '',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        $output  = '<li><i class="smicon-box-icon fa fa-' . $icon . '" style="color:' . $icon_color . '; font-size:' . $size . 'px; width:5px;"></i>';
	        $output .= '<span>' . $flitem . '</span>';
	        $output .= wpb_js_remove_wpautop($content, true);
        $output .= '</li>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/*Fancy List Holder
_______________________________________ */
class WPBakeryShortCode_fancy_list_holder extends WPBakeryShortCodesContainer {
	protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'icon' => 'none',
			'fl_list_left_margin' => '',
            'el_class' => '',
            'flitem' => '',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        $output  = '<ul class="fa-ul" style="list-style: none;" style="margin-left:' . $fl_list_left_margin . ';">';
	        $output .= wpb_js_remove_wpautop($content, true);
        $output .= '</ul>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
?>