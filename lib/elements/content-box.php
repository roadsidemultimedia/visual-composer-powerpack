<?
/* Semantic Content Box Visual Composer Element Definition
_______________________________________ */
/* Single image */
vc_map( array(
	"name" => __("Semantic Content Box w Image", "js_composer"),
	"base" => "S_content_box",
	"icon" => "icon-wpb-single-image",
	"category" => __('Content', 'js_composer'),
	"description" => __('Semantic Content Box w Image', 'js_composer'),
	"params" => array(
		array(
			"type" => "attach_image",
			"heading" => __("Image", "js_composer"),
			"param_name" => "image",
			"value" => "",
			"description" => __("Select image from media library.", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("Image size", "js_composer"),
			"param_name" => "img_size",
			"description" => __("Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use 'thumbnail' size.", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("Text Box Title", "js_composer"),
			"param_name" => "s_cb_title",
			"description" => __("Your Title under the image.", "js_composer")
		),
		array(
      "type" => "textarea_html",
      "holder" => "div",
      "heading" => __("Text", "js_composer"),
      "param_name" => "content",
      "value" => __(" ", "js_composer")
    ),
		array(
			"type" => 'checkbox',
			"heading" => __("Do you want a CTA", "js_composer"),
			"param_name" => "s_cb_cta_yn",
			"description" => __("Fill out the information below if you checked yes.", "js_composer"),
			"value" => Array(__("Yes", "js_composer") => 'yes')
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Verbage", "js_composer"),
			"param_name" => "s_cb_cta_text",
			"description" => __("", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Link", "js_composer"),
			"param_name" => "s_cb_cta_link",
			"description" => __("", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Link Color", "js_composer"),
			"param_name" => "s_cb_cta_link_color",
			"description" => __("Hex Value", "js_composer")
		),
		array(
			"type" => "dropdown",
			"heading" => __("Color of Button", "js_composer"),
			"param_name" => "s_cb_cta_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Top Margin", "js_composer"),
			"param_name" => "s_cb_cta_top_margin",
			"description" => __("Top margin value ex. 10px", "js_composer")
		),
	)
) );

/*Semantic Content Box Shortcode Output
_______________________________________ */
add_shortcode( 'S_content_box', 'semantic_content_box' );

function semantic_content_box($atts, $content = null) {

	// Set shortcode defaults
	extract( shortcode_atts( array(
	   'img_link_target' => '_self',
	   's_cb_title' => '',
	   's_cb_content' => '',
	   's_cb_cta_yn' => '',
	   's_cb_cta_text' => '',
	   's_cb_cta_color' => '',
	   's_cb_cta_link' => '',
	   's_cb_cta_link_color' => '',
	   's_cb_cta_top_margin' => '',
	), $atts ) );

	// Get image dimensions from string. Creates an array $img_dimensions[0] = width, $img_dimensions[1] = height
	$img_dimensions = explode( "x", $atts['img_size'] );
	$img = wpb_getImageBySize(array( 'attach_id' => $atts["image"], 'thumb_size' => $atts["img_size"] ));
	if ( $s_cb_cta_yn == 'yes') $s_cb_cta = '<div class="fluid ui button ' . $s_cb_cta_color . '" style="margin-top:'. $s_cb_cta_top_margin .';"><a href="'. $s_cb_cta_link .'" style="text-decoration:none; color:#'. $s_cb_cta_link_color.';">'. $s_cb_cta_text .'</a></div>';
	if ( $img == NULL ) $img['thumbnail'] = '<img src="http://placehold.it/' . $img_dimensions[0] . 'x' . $img_dimensions[1] . '/666/ffffff" />';
	$output .= '<div class="ui one items">';
	$output .= '<div class="item">';
	$output .= '<div class="image">';
	$output .= "\n\t".'<div class="'.$atts["el_class"].'">';
	$output .= "\n\t\t\t".$img['thumbnail'];
	$output .= "\n\t".'</div> ';
	$output .= '</div>';
	$output .= '<div class="content">';
	$output .= '<div class="name">'. $s_cb_title .'</div>';
	$output .= $content;
	$output .= $s_cb_cta;
    $output .= '</div></div></div>';
	
	return $output;
}
/* Semantic Content Box Visual Composer Element Definition
_______________________________________ */
vc_map( array(
	"name" => __("Semantic Content Box", "js_composer"),
	"base" => "S_content_box_no_img",
	"icon" => "icon-wpb-single-image",
	"category" => __('Content', 'js_composer'),
	"description" => __('Semantic Content Box w Image', 'js_composer'),
	"params" => array(
		array(
			"type" => "textfield",
			"heading" => __("Text Box Title", "js_composer"),
			"param_name" => "s_cb_title",
			"description" => __("Your Title under the image.", "js_composer")
		),
		array(
      "type" => "textarea_html",
      "holder" => "div",
      "heading" => __("Text", "js_composer"),
      "param_name" => "content",
      "value" => __(" ", "js_composer")
    ),
		array(
			"type" => 'checkbox',
			"heading" => __("Do you want a CTA", "js_composer"),
			"param_name" => "s_cb_cta_yn",
			"description" => __("Fill out the information below if you checked yes.", "js_composer"),
			"value" => Array(__("Yes", "js_composer") => 'yes')
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Verbage", "js_composer"),
			"param_name" => "s_cb_cta_text",
			"description" => __("", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Link", "js_composer"),
			"param_name" => "s_cb_cta_link",
			"description" => __("", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Link Color", "js_composer"),
			"param_name" => "s_cb_cta_link_color",
			"description" => __("Hex Value", "js_composer")
		),
		array(
			"type" => "dropdown",
			"heading" => __("Color of Button", "js_composer"),
			"param_name" => "s_cb_cta_color",
			"value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
			"description" => __("Choose a color any color", "js_composer")
		),
		array(
			"type" => "textfield",
			"heading" => __("CTA Top Margin", "js_composer"),
			"param_name" => "s_cb_cta_top_margin",
			"description" => __("Top margin value ex. 10px", "js_composer")
		),
	)
) );

/*Semantic Content Box Shortcode Output
_______________________________________ */
add_shortcode( 'S_content_box_no_img', 'semantic_content_box_no_img' );

function semantic_content_box_no_img($atts, $content = null) {


	// Set shortcode defaults
	extract( shortcode_atts( array(
	   's_cb_title' => '',
	   's_cb_content' => '',
	   's_cb_cta_yn' => '',
	   's_cb_cta_text' => '',
	   's_cb_cta_color' => '',
	   's_cb_cta_link' => '',
	   's_cb_cta_link_color' => '',
	   's_cb_cta_top_margin' => '',
	), $atts ) );

	$output .= '<div class="ui one items">';
	$output .= '<div class="item">';
	$output .= '<div class="content">';
	$output .= '<div class="name">'. $s_cb_title .'</div>';
	$output .= $content;
	$output .= $s_cb_cta;
    $output .= '</div></div></div>';
	
	return $output;
}
?>