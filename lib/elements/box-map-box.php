<?
/* Box Map Box Holder
_________________________________________ */
vc_map( array(
    "name" => __("Box Map Box Holder", "js_composer"),
    "base" => "box_map_box_holder",
    "icon" => "icon-wpb-box",
    "as_parent" => array('only' => 'box_map_box, box_map_box_side_box,box_map_box_big_hdr'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        )
    ),
    "js_view" => 'VcColumnView'
) );
/* Box Map Box
_______________________________________ */
vc_map( array(
    "name" => __("BOX-MAP-BOX", "js_composer"),
    "base" => "box_map_box",
	"icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "as_child" => array('only' => 'box_map_box_holder'),
    "params" => array(
        // add params same as with any other content element
		array(
            "type" => 'checkbox',
            "heading" => __("POPOUT", "js_composer"),
            "param_name" => "bmb_pop",
            "description" => __("Do you want this box to pop?", "js_composer"),
            "value" => Array(__("Yes", "js_composer") => 'yes')
        ),
        array(
            "type" => "textfield",
            "heading" => __("Map Title", "js_composer"),
            "param_name" => "bmb_title",
            "description" => __("", "js_composer")
        ),
		array(
            "type" => "textfield",
            "heading" => __("Latitude", "js_composer"),
            "param_name" => "bmb_lat",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Longitude", "js_composer"),
            "param_name" => "bmb_long",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Zoom", "js_composer"),
            "param_name" => "bmb_zoom",
            "description" => __("Zoom level", "js_composer")
        ),
		array(
            "type" => "textfield",
            "heading" => __("Icon Link", "js_composer"),
            "param_name" => "bmb_icon",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Box Height", "js_composer"),
            "param_name" => "bmb_height",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Main Text", "js_composer"),
            "param_name" => "bmb_cta_main",
            "description" => __("Text for the left side of the box.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Descriptive Text", "js_composer"),
            "param_name" => "bmb_cta_desc",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Color of button", "js_composer"),
            "param_name" => "bmb_cta_color",
            "value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
            "description" => __("Choose a color any color", "js_composer")
        ),
    )
) );
/* Box Map Box
_______________________________________ */
class WPBakeryShortCode_box_map_box extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			'bmb_title' => '',
            'bmb_pop' => '',
            'bmb_zoom' => '',
			'bmb_lat' => '',
			'bmb_long' => '',
			'bmb_icon' => '',
            'bmb_cta_main' => '',
            'bmb_cta_desc' => '',            
            'bmb_height' => '',
            'bmb_cta_main' => '',
            'bmb_cta_desc' => '',
            'bmb_cta_color' => '',
			
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        if ($bmb_pop == 'yes') $popclass = 'bmb_blockhover';
	        $output = '<script type="text/javascript">';
			$output .= "\n".'function boxmapbox() {';
			$output .= "\n".'	var latlng = new google.maps.LatLng('. $bmb_lat .', '. $bmb_long .');';
			$output .= "\n".'	var settings = {';
			$output .= "\n".'		zoom: '. $bmb_zoom .',';
			$output .= "\n".'		center: latlng,';
			$output .= "\n".'		mapTypeControl: true,';
			$output .= "\n".'		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},';
			$output .= "\n".'		navigationControl: true,';
			$output .= "\n".'		navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},';
			$output .= "\n".'		mapTypeId: google.maps.MapTypeId.ROADMAP};';
			$output .= "\n".'	var map = new google.maps.Map(document.getElementById("bmb_map"), settings);';
			$output .= "\n".'';
			$output .= "\n"."	var companyImage = new google.maps.MarkerImage('". $bmb_icon ."',";
			$output .= "\n".'		new google.maps.Size(100,50),';
			$output .= "\n".'		new google.maps.Point(0,0),';
			$output .= "\n".'		new google.maps.Point(50,50)';
			$output .= "\n".'	);';
			$output .= "\n"."	var companyShadow = new google.maps.MarkerImage('images/logo_shadow.png',";
			$output .= "\n".'		new google.maps.Size(130,50),';
			$output .= "\n".'		new google.maps.Point(0,0),';
			$output .= "\n".'		new google.maps.Point(65, 50));';
			$output .= "\n".'	var companyPos = new google.maps.LatLng('. $bmb_lat .', '. $bmb_long .');';
			$output .= "\n".'	var companyMarker = new google.maps.Marker({';
			$output .= "\n".'		position: companyPos,';
			$output .= "\n".'		map: map,';
			$output .= "\n".'		icon: companyImage,';
			$output .= "\n".'		shadow: companyShadow,';
			$output .= "\n".'		title:"Roadside",';
			$output .= "\n".'		zIndex: 3});';
			$output .= "\n"."					google.maps.event.addListener(companyMarker, 'click', function() {";
			$output .= "\n".'		infowindow.open(map,companyMarker);';
			$output .= "\n".'	});';
			$output .= "\n".'} window.onload = boxmapbox;';
		    $output .= "\n".'</script>';
		    $output .= "\n".'<li class="bmb_block '. $popclass .'"><div id="bmb_map" style="width:100%; height:'. $bmb_height .'"></div>';
            $output .= "\n".'<div class="ui circular fluid '. $bmb_cta_color .' button" style="margin: -20px 0px 0px 10%; width:80%; z-index:100; position:absolute;">';
            $output .= "\n".$bmb_cta_main;
            $output .= "\n".'<div class="descriptive_button" style="font-size:10px;">' . $bmb_cta_desc . '</div>';
            $output .= "\n".'</div></li>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/* Box Map Box Left
_______________________________________ */
vc_map( array(
    "name" => __("BOX-MAP-BOX Side Box", "js_composer"),
    "base" => "box_map_box_side_box",
    "icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "as_child" => array('only' => 'box_map_box_holder'),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => 'checkbox',
            "heading" => __("POPOUT", "js_composer"),
            "param_name" => "bmb_pop",
            "description" => __("Do you want this box to pop?", "js_composer"),
            "value" => Array(__("Yes", "js_composer") => 'yes')
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "js_composer"),
            "param_name" => "bmb_title",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "colorpicker",
            "heading" => __("HDR BG Color", "js_composer"),
            "param_name" => "bmb_hdr_color",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textarea_html",
            "heading" => __("Content", "js_composer"),
            "param_name" => "content",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Box Height", "js_composer"),
            "param_name" => "bmb_height",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "colorpicker",
            "heading" => __("BOX BG Color", "js_composer"),
            "param_name" => "bmb_box_color",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Main Text", "js_composer"),
            "param_name" => "bmb_cta_main",
            "description" => __("Text for the left side of the box.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Descriptive Text", "js_composer"),
            "param_name" => "bmb_cta_desc",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Color of button", "js_composer"),
            "param_name" => "bmb_cta_color",
            "value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
            "description" => __("Choose a color any color", "js_composer")
        ),
    )
) );
/* Box Map Box Left
_______________________________________ */
class WPBakeryShortCode_box_map_box_side_box extends WPBakeryShortCode {
        protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'bmb_pop' => '',
            'bmb_title' => '',
            'bmb_hdr_color' => '',
            'bmb_height' => '',
            'bmb_box_color' => '',
            'bmb_cta_main' => '',
            'bmb_cta_desc' => '',
            'bmb_cta_color' => '',
        ), $atts));
        if ($bmb_pop == 'yes') $popclass = 'bmb_blockhover';

            $output = "\n".'<li class="bmb_block '. $popclass .'">';
            $output .= "\n".'<div class="bmb">';
            $output .= "\n".'    <div class="bmb_figure" style="background-color:'. $bmb_hdr_color .'; ">';
            $output .= "\n".'        <span class="bmb_tenure">'. $bmb_title .'</div></div>';
            $output .= "\n".'<div class="features" style="background-color:'. $bmb_box_color .'; height:'. $bmb_height .'; ">';
            $output .= wpb_js_remove_wpautop($content, true);
            $output .= "\n".'</div><div class="ui circular fluid '. $bmb_cta_color .' button" style="margin: -5px 0px 0px 10%; width:80%; z-index:100; position:absolute;">';
            $output .= $bmb_cta_main;
            $output .= "\n".'    <div class="descriptive_button" style="font-size:10px;">'. $bmb_cta_desc .'</div>';
            $output .= "\n".'    </div>';
            $output .= "\n".'    <div class="drkfooter" style="background-color:'. $bmb_hdr_color .'; ">';
            $output .= "\n".'    </div>';
            $output .= "\n".'</li>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/* Box Map Box Left
_______________________________________ */
vc_map( array(
    "name" => __("BMB - BIG HDR", "js_composer"),
    "base" => "box_map_box_big_hdr",
    "icon" => "icon-wpb-s-button",
    "content_element" => true,
    "show_settings_on_create" => true,
    "as_child" => array('only' => 'box_map_box_holder'),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => 'checkbox',
            "heading" => __("POPOUT", "js_composer"),
            "param_name" => "bmb_pop",
            "description" => __("Do you want this box to pop?", "js_composer"),
            "value" => Array(__("Yes", "js_composer") => 'yes')
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "js_composer"),
            "param_name" => "bmb_title",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "colorpicker",
            "heading" => __("HDR BG Color", "js_composer"),
            "param_name" => "bmb_hdr_color",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Box Height", "js_composer"),
            "param_name" => "bmb_height",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Main Text", "js_composer"),
            "param_name" => "bmb_cta_main",
            "description" => __("Text for the left side of the box.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "heading" => __("CTA - Descriptive Text", "js_composer"),
            "param_name" => "bmb_cta_desc",
            "description" => __("", "js_composer")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Color of button", "js_composer"),
            "param_name" => "bmb_cta_color",
            "value" => array('White' => '', 'Black' => 'black', 'Green' => 'green', 'Red' => 'red', 'Blue' => 'blue', 'Purple' => 'purple', 'Teal' => 'teal', 'Orange' => 'orange'),
            "description" => __("Choose a color any color", "js_composer")
        ),
    )
) );
/* Box Map Box BIG HDR
_______________________________________ */
class WPBakeryShortCode_box_map_box_big_hdr extends WPBakeryShortCode {
        protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'bmb_pop' => '',
            'bmb_title' => '',
            'bmb_hdr_color' => '',
            'bmb_height' => '',
            'bmb_cta_main' => '',
            'bmb_cta_desc' => '',
            'bmb_cta_color' => '',
        ), $atts));
        if ($bmb_pop == 'yes') $popclass = 'bmb_blockhover';

            $output = "\n".'<li class="bmb_block '. $popclass .'">';
            $output .= "\n".'<div class="bmb">';
            $output .= "\n".' <div class="bmb_figure" style="background-color:'. $bmb_hdr_color .'; height: '. $bmb_height .' ">';
            $output .= "\n".'        <span class="bmb_tenure">'. $bmb_title .'</div></div>';
            $output .= "\n".'<div class="ui circular fluid '. $bmb_cta_color .' button" style="margin: -5px 0px 0px 10%; width:80%; z-index:100; position:absolute;">';
            $output .= $bmb_cta_main;
            $output .= "\n".'    <div class="descriptive_button" style="font-size:10px;">'. $bmb_cta_desc .'</div>';
            $output .= "\n".'    </div>';
            $output .= "\n".'</li>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/*Box Holder
_______________________________________ */
class WPBakeryShortCode_box_map_box_holder extends WPBakeryShortCodesContainer {
    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'el_class' => '',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        $output  = '<ul class="bmb_table">';
            $output .= wpb_js_remove_wpautop($content, true);
        $output .= '</ul>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

?>