<?
/* Semantic Animated Button 
_______________________________________ */
vc_map( array(
    "name" => __("Block Quote", "js_composer"),
    "base" => "block_quote",
	"icon" => "icon-wpb-s-button",
    "admin_label" => true,
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
		array(
            "type" => 'dropdown',
            "heading" => __("Blockquote Style", "js_composer"),
            "param_name" => "block_quote_style",
            "description" => __("Only Check One", "js_composer"),
            "value" => Array(
                __("Primary", "js_composer") => 'primary',
                __("Secondary", "js_composer") => 'secondary', 
                __("Tertiary", "js_composer") => 'tertiary',
            )
        ),
		array(
			"type" => "textarea_html",
			"heading" => __("Quote Content", "js_composer"),
			"param_name" => "content",
			"description" => __(" ", "js_composer")
		),
        array(
            "type" => "textfield",
            "heading" => __("Quote Author", "js_composer"),
            "param_name" => "block_quote_author",
            "description" => __(" ", "js_composer")
        )
    ),
) );
/*Semantic Animated
_______________________________________ */
class WPBakeryShortCode_block_quote extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
			'block_quote_style' => '',
            'block_quote_author' => '',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
	        $output = '<blockquote class="'. $block_quote_style .'">';
	        $output .= '<p>'. $content .'</p>';
            $output .= '<footer><cite>'. $block_quote_author .'</footer></cite>';
	        $output .= '</blockquote>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

?>