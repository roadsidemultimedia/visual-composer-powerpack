<?
/* Masonry
-------------------------------------------- */
/* Masonry Textbox
---------------------------------------------------------- */
vc_map( array(
    "name"		=> __("Masonry Textbox", "js_composer"),
	"base"		=> "masonry_textbox",
    "class"		=> "",
    "icon" => "icon-wpb-masonry",
	"wrapper_class" => "clearfix",
	"content_element" => true,
    "params"	=> array(
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Text", "js_composer"),
            "param_name" => "content",
            "value" => __(" ", "js_composer"),
            "description" => __("Enter your content.", "js_composer")
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Width", "js_composer"),
			"param_name" => "fgwidth",
			"value" => array('1' => '1', '2' => '2', '3' => '3'),
			"description" => __("The respective size of your box. 1 thru 3.", "js_composer")
		),
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        ),
    )
) );

/* Masonry Container
_______________________________________ */
vc_map( array(
    "name" => __("Masonry Holder", "js_composer"),
    "base" => "masonry_holder",
	"icon" => "icon-wpb-masonry",
    "as_parent" => array('only' => 'masonry_textbox'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
        )
    ),
    "js_view" => 'VcColumnView'
) );

/*Masonry Grid Holder
_______________________________________ */
class WPBakeryShortCode_masonry_holder extends WPBakeryShortCodesContainer {
	protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'el_class' => '',
        ), $atts));
        $output  = '<div class="demos"><section id="content"><div id="tumblelog" class="clearfix">';
	        $output .= wpb_js_remove_wpautop($content, true);
        $output .= '</section></div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
/*Masonry Content Box
_______________________________________ */
class WPBakeryShortCode_masonry_textbox extends WPBakeryShortCode {
	    protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'el_class' => '',
			'fgwidth' => '',
        ), $atts));
        $css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width_class, $this->settings['base']);
        $output  = '<div class="story col' . $fgwidth . '">';
		$content = wpb_js_remove_wpautop($content); // fix unclosed/unwanted paragraph tags in $content
	    $output .= '' . $content . '';
        $output .= '</div>';
        
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

?>