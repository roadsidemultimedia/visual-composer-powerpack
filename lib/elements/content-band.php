<?
/* -------------------------------------------- */
/* Content Band
---------------------------------------------------------- */
vc_map( array(
    "name" => __("Content Band", "js_composer"),
    "base" => "content_band",
	"icon" => "icon-wpb-vc_carousel",
    "as_parent" => array('all' => 'true'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "as_parent" => true, // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,

    // HTML tag name where Visual Composer will store attribute value in Visual Composer edit mode. Default: hidden input
    // "holder" => "test",

    "params" => array(
        // add params same as with any other content element
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "js_composer"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer"),
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Choose a template", "js_composer"),
            "param_name" => "template",
            "value" => array('Primary', 'Secondary', 'Tertiary'),
            "description" => __("Choose a template. The templates are configured in the Visual Composer Power Pack admin page.", "js_composer"),
        ),
        array(
            "type" => "checkbox",
            "heading" => __("Customize?", "js_composer"),
            "param_name" => "customize_enable",
            "description" => __("Choose a template. The templates are configured in the Visual Composer Power Pack admin page.", "js_composer"),
            "value" => Array(__("Yes", "js_composer") => 'yes'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __("Full Width?", "js_composer"),
            "param_name" => "full_width_enable",
            "description" => __("Do you want to make this panel full width?", "js_composer"),
            "value" => Array(__("Yes", "js_composer") => 'yes'),
            "dependency" => array(
                "element" => "customize_enable", 
                "value" => "yes",
            ),
        ),
        array(
            "type" => "attach_image",
            "heading" => __("Image", "js_composer"),
            "param_name" => "image",
            "value" => "",
            "description" => __("Select a background image", "js_composer"),
            "dependency" => array(
                "element" => "customize_enable", 
                "value" => "yes",
            ),
        ),
    ),
    "js_view" => 'VcColumnView'
) );

/*Masonry Grid Holder
_______________________________________ */
// class WPBakeryShortCode_masonry_holder extends WPBakeryShortCodesContainer {
// 	protected function content($atts, $content = null) {

//         extract(shortcode_atts(array(
//             'el_class' => '',
//         ), $atts));
//         $output  = '<div class="demos"><section id="content"><div id="tumblelog" class="clearfix">';
// 	        $output .= wpb_js_remove_wpautop($content, true);
//         $output .= '</section></div>';
        
//         $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
//         return $output;
//     }
// }


?>