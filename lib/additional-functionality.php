<?php

// @TODO
// Add a admin page with the option of enabling or disabling this feature.
// Until then, enable in the theme.


/* ----------------------------------------------------------
 * Removes default H1 if one is found in the content field
 *
 * @TODO
 * Detect when VC Heading Element uses an H1 tag as well
 * ---------------------------------------------------------- */
/*add_action( 'template_redirect', 'check_for_page_title_shortcode' );
function check_for_page_title_shortcode() { 
  global $wp_query;
  if ( is_page() ) {
    $post = $wp_query->get_queried_object();
    if ( false !== strpos($post->post_content, '<h1') ) {
      add_filter( 'hybrid_entry_title', '__return_false' );
    }
  }
}
*/
/* ----------------------------------------------------------
 * Add classes to the body tag to show if 
 * Visual Composer is being used on that page
 * ---------------------------------------------------------- */
add_action( 'template_redirect', 'vc_after_theme_setup', 11 );
function vc_after_theme_setup(){
  global $wp_query;
  $post = $wp_query->get_queried_object();

  if( strpos($post->post_content, 'vc_row') ){
    add_filter('body_class', 'active_vc_classes');
    function active_vc_classes($classes){
      $classes[] = 'vc_active';
      return array_unique($classes);
    };
  } else {
    add_filter('body_class', 'inactive_vc_classes');
    function inactive_vc_classes($classes){
      $classes[] = 'vc_inactive';
      return array_unique($classes);
    };
  }
}


/* ----------------------------------------------------------
 * Fix Content Block Custom Post Type to allow use with Visual Composer
 * Adds 'public' => true so that Visual Composer sees it as a valid post type
 * ---------------------------------------------------------- */
function register_my_post_type() {
  register_post_type( "content_block",
    array(
      'labels' => array(
        'name' => __( 'Content block', 'hybrid' ),
        'singular_name' => __( 'Block', 'hybrid' ),
        'add_new' => __( 'Add Block', 'hybrid' ),
        'add_new_item' => __( 'Add New Block', 'hybrid' ),
        'edit' => __( 'Edit', 'hybrid' ),
        'edit_item' => __( 'Edit Block', 'hybrid' ),
        'new_item' => __( 'New Block', 'hybrid' ),
        'view' => __( 'View Block', 'hybrid' ),
        'view_item' => __( 'View Block', 'hybrid' ),
        'search_items' => __( 'Search Blocks', 'hybrid' ),
        'not_found' => __( 'No Blocks found', 'hybrid' ),
        'not_fontund_in_trash' => __( 'No Blocks found in trash', 'hybrid' ),
        'parent' => __( 'Parent Block', 'hybrid' )
      ),
      'public' => true
    )
  );
}
add_action( 'init', 'register_my_post_type', 10000 );
// 1000 -> lower priority, will run after the plugin init action
