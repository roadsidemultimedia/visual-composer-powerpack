<?php


/* Grid JS 
------------------------------------------------------------------- */

add_action('wp_enqueue_scripts','enqueue_vcpp_grid_script');
function enqueue_vcpp_grid_script() {
    wp_register_script( 'masonry', plugins_url( '/assets/js/jquery.masonry.js', __FILE__ ), false, 1.0, true  );
    wp_enqueue_script( 'masonry' );
}
add_action('wp_enqueue_scripts','enqueue_vcpp_grid');
function enqueue_vcpp_grid() {
    wp_register_script( 'startmasonry', plugins_url( '/assets/js/masonry.js', __FILE__ ), false, 1.0, true  );
    wp_enqueue_script( 'startmasonry' );
}
/* Semantic 
------------------------------------------------------------------- */

add_action('wp_enqueue_scripts','enqueue_vcpp_semantic_script');
function enqueue_vcpp_semantic_script() {
    wp_register_script( 'semantic', plugins_url( '/assets/js/semantic.min.js', __FILE__ ), false, 1.0, true  );
    wp_enqueue_script( 'semantic' );
}
add_action( 'wp_print_styles', 'enqueue_vcpp_semanitc_styles' );
function enqueue_vcpp_semanitc_styles(){
 wp_register_style( 'vcpp_semanitc_styles', plugins_url( '/assets/css/semantic.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_semanitc_styles' );
}
/* Extra 
------------------------------------------------------------------- */
add_action( 'wp_print_styles', 'enqueue_vcpp_admin_styles' );
function enqueue_vcpp_admin_styles(){
 wp_register_style( 'vcpp_admin_styles', plugins_url( '/assets/css/vc-admin-extend.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_admin_styles' );
}
add_action('wp_enqueue_scripts','enqueue_vcpp_gmap_script');
function enqueue_vcpp_gmap_script() {
    wp_register_script( 'gmap', 'http://maps.google.com/maps/api/js?sensor=false' );
    wp_enqueue_script( 'gmap' );
}
/* OWL CAROUSEL 
------------------------------------------------------------------- */
add_action( 'wp_print_styles', 'enqueue_vcpp_owl_carousel' );
function enqueue_vcpp_owl_carousel(){
 wp_register_style( 'vcpp_owl_carousel_styles', plugins_url( '/assets/css/owl.carousel.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_owl_carousel_styles' );
}
add_action( 'wp_print_styles', 'enqueue_vcpp_owl_theme' );
function enqueue_vcpp_owl_theme(){
 wp_register_style( 'vcpp_owl_theme_styles', plugins_url( '/assets/css/owl.theme.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_owl_theme_styles' );
}
add_action('wp_enqueue_scripts','enqueue_vcpp_owl_script');
function enqueue_vcpp_owl_script() {
    wp_register_script( 'owlcarousel', plugins_url( '/assets/js/owl.carousel.js', __FILE__ ), false, 1.0, true );
    wp_enqueue_script( 'owlcarousel' );
}
/* Main page css 
------------------------------------------------------------------- */
add_action( 'wp_print_styles', 'enqueue_vcpp_styles' );
function enqueue_vcpp_styles(){
 wp_register_style( 'vcpp_styles', plugins_url( '/assets/css/vcpp-frontend.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_styles' );
}
add_action( 'wp_print_styles', 'enqueue_vcpp_gridstyles' );
function enqueue_vcpp_gridstyles(){
 wp_register_style( 'vcpp_gridstyles', plugins_url( '/assets/css/grid.css', __FILE__ ), false, 1.0 );
 wp_enqueue_style( 'vcpp_gridstyles' );
}
?>