<?
$this->vcpp_sections[] = array(
    'title' => __('Blockquote Settings', 'visual-composer-power-pack'),
    'desc' => __('<p>You can configure the blockquote settings on this page. These styles are used throughout the site.</p>', 'visual-composer-power-pack'),
    'icon' => 'el-icon-quotes',
    'fields' => array(

        ////////////////////////
        // Primary Block Quote
        ////////////////////////
        array(
            'id' => 'blockquote-primary-section',
            'type' => 'section',
            'title' => __('Primary Blockquote', 'visual-composer-power-pack'),
            'indent' => true // Indent all options below until the next 'section' option is set.
        ),
        // array(
        //                 'id' => 'blockquote-primary-color-background',
        //                 'type' => 'background',
        //                 'output' => array('blockquote.primary'),
        //                 'title' => __('Blockquote Background Color', 'redux-framework-demo'),
        //                 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
        //                 //'default' => '#FFFFFF',
        //             ),
        array(
                        'id' => 'blockquote-primary-spacing',
                        'type' => 'spacing',
                        'output' => array('blockquote.primary'), // An array of CSS selectors to apply this font style to
                        'mode' => 'margin', // absolute, padding, margin, defaults to padding
                        'top' => false, // Disable the top
                        'right' => false, // Disable the right
                        'bottom' => false, // Disable the bottom
                        //'left' => false, // Disable the left
                        //'all' => true, // Have one field that applies to all
                        'units' => 'px', // You can specify a unit value. Possible: px, em, %
                        //'units_extended' => 'true', // Allow users to select any type of unit
                        //'display_units' => 'false', // Set to false to hide the units if the units are specified
                        'title' => __('Left Margin', 'visual-composer-power-pack'),
                        'subtitle' => __('Allow your users to choose the left margin for the blockquote.', 'redux-framework-demo'),
                    ),
        array(
            'id' => 'blockquote-primary-typography',
            'title' => __('Blockquote Typography', 'visual-composer-power-pack'),
            'type' => 'typography',
            'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
            'font-backup' => false, // Select a backup non-google font in addition to a google font
            'subsets'=>false, // Only appears if google is true and subsets not set to false
            'word-spacing'=>true, // Defaults to false
            'letter-spacing'=>true, // Defaults to false
            'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            'units' => 'em', // Defaults to px
            'subtitle' => __('Typography option with each property can be called individually.', 'visual-composer-power-pack'),
            'default' => array(
                'color' => "#333",
                'font-style' => '400',
                'font-family' => 'Crimson Text',
                'google' => true,
                'font-size' => '2',
                'line-height' => '2.2'
            ),
            'output' => array('blockquote.primary'), // An array of CSS selectors to apply this font style to dynamically
        ),

        ////////////////////////
        // Secondary Block Quote
        ////////////////////////
        
        array(
            'id' => 'blockquote-secondary-section',
            'type' => 'section',
            'title' => __('Secondary Blockquote', 'visual-composer-power-pack'),
            'indent' => true // Indent all options below until the next 'section' option is set.
        ),
        // array(
        //                 'id' => 'blockquote-secondary-color-background',
        //                 'type' => 'background',
        //                 'output' => array('blockquote.secondary'),
        //                 'title' => __('Blockquote Background Color', 'redux-framework-demo'),
        //                 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'visual-composer-power-pack'),
        //                 'default' => '#FFFFFF',
        //                 'validate' => 'color',
        //             ),
        array(
                        'id' => 'blockquote-secondary-quote-color',
                        'type' => 'color',
                        'output' => array('blockquote.secondary::before'),
                        'title' => __('Blockquote Quote Color', 'redux-framework-demo'),
                        'subtitle' => __('Pick a background color for the theme (default: #fff).', 'visual-composer-power-pack'),
                        'default' => '#FFFFFF',
                        'validate' => 'color',
                    ),
        array(
                        'id' => 'blockquote-secondary-spacing',
                        'type' => 'spacing',
                        'output' => array('blockquote.secondary'), // An array of CSS selectors to apply this font style to
                        'mode' => 'margin', // absolute, padding, margin, defaults to padding
                        'top' => false, // Disable the top
                        'right' => false, // Disable the right
                        'bottom' => false, // Disable the bottom
                        //'left' => false, // Disable the left
                        //'all' => true, // Have one field that applies to all
                        'units' => 'px', // You can specify a unit value. Possible: px, em, %
                        //'units_extended' => 'true', // Allow users to select any type of unit
                        //'display_units' => 'false', // Set to false to hide the units if the units are specified
                        'title' => __('Left Margin', 'visual-composer-power-pack'),
                        'subtitle' => __('Allow your users to choose the left margin for the blockquote.', 'visual-composer-power-pack'),
                        'default' => array('margin-left' => '30px'),
                    ),
        array(
            'id' => 'blockquote-secondary-typography',
            'title' => __('Blockquote Typography', 'visual-composer-power-pack'),
            'type' => 'typography',
            'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
            'font-backup' => false, // Select a backup non-google font in addition to a google font
            'subsets'=>false, // Only appears if google is true and subsets not set to false
            'word-spacing'=>true, // Defaults to false
            'letter-spacing'=>true, // Defaults to false
            'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            'units' => 'em', // Defaults to px
            'subtitle' => __('Typography option with each property can be called individually.', 'visual-composer-power-pack'),
            'default' => array(
                'color' => "#333",
                'font-style' => '400',
                'font-family' => 'Arvo',
                'google' => true,
                'font-size' => '2',
                'line-height' => '2.2'
            ),
            'output' => array('blockquote.secondary'),
        ),
        
        ////////////////////////
        // Tertiary Block Quote
        ////////////////////////
        array(
            'id' => 'blockquote-tertiary-section',
            'type' => 'section',
            'title' => __('Tertiary Blockquote', 'visual-composer-power-pack'),
            'indent' => true // Indent all options below until the next 'section' option is set.
        ),
        // array(
        //                 'id' => 'blockquote-tertiary-color-background',
        //                 'type' => 'background',
        //                 'output' => array('blockquote.tertiary'),
        //                 'title' => __('Blockquote Background Color', 'redux-framework-demo'),
        //                 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
        //                 'default' => '#FFFFFF',
        //                 'validate' => 'color',
        //             ),
        array(
                        'id' => 'blockquote-tertiary-spacing',
                        'type' => 'spacing',
                        'output' => array('blockquote.tertiary'), // An array of CSS selectors to apply this font style to
                        'mode' => 'margin', // absolute, padding, margin, defaults to padding
                        'top' => false, // Disable the top
                        'right' => false, // Disable the right
                        'bottom' => false, // Disable the bottom
                        //'left' => false, // Disable the left
                        //'all' => true, // Have one field that applies to all
                        'units' => 'px', // You can specify a unit value. Possible: px, em, %
                        //'units_extended' => 'true', // Allow users to select any type of unit
                        //'display_units' => 'false', // Set to false to hide the units if the units are specified
                        'title' => __('Left Margin', 'visual-composer-power-pack'),
                        'subtitle' => __('Allow your users to choose the left margin for the blockquote.', 'redux-framework-demo'),
                        'default' => array('margin-left' => '30px'),
                    ),
        array(
            'id' => 'blockquote-tertiary-typography',
            'title' => __('Blockquote Typography', 'visual-composer-power-pack'),
            'type' => 'typography',
            'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
            'font-backup' => false, // Select a backup non-google font in addition to a google font
            'subsets'=>false, // Only appears if google is true and subsets not set to false
            'word-spacing'=>true, // Defaults to false
            'letter-spacing'=>true, // Defaults to false
            'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            'units' => 'em', // Defaults to px
            'subtitle' => __('Typography option with each property can be called individually.', 'visual-composer-power-pack'),
            'default' => array(
                'color' => "#333",
                'font-style' => '400',
                'font-family' => 'Oswald',
                'google' => true,
                'font-size' => '2',
                'line-height' => '2.2'
            ),
            'output' => array('blockquote.tertiary'),
        ),

    ),
);
?>