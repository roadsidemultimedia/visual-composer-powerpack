<?php

if (!class_exists("Redux_Framework_vcpp_config")) {

    class Redux_Framework_vcpp_config {

        public $vcpp_args = array();
        public $vcpp_sections = array();
        public $vcpp_theme;
        public $ReduxFramework;

        public function __construct() {

            if ( !class_exists("ReduxFramework" ) ) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if ( defined('TEMPLATEPATH') && strpos( Redux_Helpers::cleanFilePath( __FILE__ ), Redux_Helpers::cleanFilePath( TEMPLATEPATH ) ) !== false) {
                $this->initVCPPSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initVCPPSettings'), 10);
            }
        }

        public function initVCPPSettings() {

            // Set the default arguments
            $this->setVCPPArguments();

            // Create the sections and fields
            $this->setVCPPSections();

            if (!isset($this->vcpp_args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->vcpp_sections, $this->vcpp_args);
        }

        public function setVCPPSections() {

            // ACTUAL DECLARATION OF SECTIONS

            $this->vcpp_sections[] = array(
                'title' => __('Visual Composer Power Pack Settings', 'visual-composer-power-pack'),
                'desc' => __('<p>You can configure the options and styling of various elements.</p>', 'visual-composer-power-pack'),
                'icon' => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields' => array(

                    array(
                        'id' => 'typography',
                        'type' => 'typography',
                        'title' => __('Typography', 'visual-composer-power-pack'),
                        //'compiler'=>true, // Use if you want to hook in your own CSS compiler
                        'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup' => true, // Select a backup non-google font in addition to a google font
                        //'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        //'subsets'=>false, // Only appears if google is true and subsets not set to false
                        //'font-size'=>false,
                        //'line-height'=>false,
                        //'word-spacing'=>true, // Defaults to false
                        //'letter-spacing'=>true, // Defaults to false
                        //'color'=>false,
                        //'preview'=>false, // Disable the previewer
                        'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
                        'output' => array('h2.site-description'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler' => array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units' => 'px', // Defaults to px
                        'subtitle' => __('Typography option with each property can be called individually.', 'visual-composer-power-pack'),
                        'default' => array(
                            'color' => "#333",
                            'font-style' => '700',
                            'font-family' => 'Abel',
                            'google' => true,
                            'font-size' => '33px',
                            'line-height' => '40px'),
                    ),

                ),
            );
            
            // Auto load all php files in the lib/elements-admin folder
            foreach ( glob( dirname( __FILE__ ) . "/elements-admin/*" ) as $filename) { 
                include_once( $filename );
            }


        }

        public function setVCPPArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->vcpp_args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name' => 'visual_composer_powerpack_options', // This is where your data is stored in the database and also becomes your global variable name.
                'page_slug' => '_vcpp', // Page slug used to denote the panel
                'display_name' => 'Visual Composer Power Pack Settings', // Name that appears at the top of your panel
                'display_version' => VISUAL_COMPOSER_POWERPACK_VERSION, // Version that appears at the top of your panel
                'menu_type' => 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu' => true, // Show the sections below the admin menu item or not
                'menu_title' => __('VC Power Pack', 'visual-composer-power-pack'),
                'page_title' => __('Visual Composer Power Pack Settings', 'visual-composer-power-pack'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyBr-nPw6GoHVdh2OBqvzRrxy3NDkDOkdog', // Must be defined to add google fonts to the typography module
                
                //'async_typography' => false, // Use a asynchronous font on the front end or font string
                'admin_bar' => false, // Show the panel pages on the admin bar
                'global_variable' => '', // Set a different name for your global variable other than the opt_name
                'dev_mode' => true, // Show the time the page took to load, etc
                'customizer' => true, // Enable basic customizer support
                // OPTIONAL -> Give you extra features
                'page_priority' => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent' => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions' => 'manage_options', // Permissions needed to access the options panel.
                'menu_icon' => '', // Specify a custom URL to an icon
                'last_tab' => '', // Force your panel to always open to a specific tab (by id)
                'page_icon' => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
                'save_defaults' => true, // On load save the defaults to DB before user clicks save or not
                'default_show' => false, // If true, shows the default value next to each field that is not the default value.
                'default_mark' => '', // What to print by the field's title if the value shown is default. Suggested: *
                // CAREFUL -> These options are for advanced use only
                'transient_time' => 60 * MINUTE_IN_SECONDS,
                'output' => true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag' => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit' => false, // Disable the footer credit of Redux. Please leave if you can help it.
                'database' => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'show_import_export' => true, // REMOVE
                'system_info' => false, // REMOVE
                'help_tabs' => array(),
                'help_sidebar' => '', // __( '', $this->vcpp_args['domain'] );
                'hints' => array(
                    'icon'              => 'icon-question-sign',
                    'icon_position'     => 'right',
                    'icon_color'        => 'lightgray',
                    'icon_size'         => 'normal',

                    'tip_style'         => array(
                        'color'     => 'light',
                        'shadow'    => true,
                        'rounded'   => false,
                        'style'     => '',
                    ),
                    'tip_position'      => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect' => array(
                        'show' => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'mouseover',
                        ),
                        'hide' => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );

        }

    }

    new Redux_Framework_vcpp_config();
}